package com.example.testy;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.os.AsyncTask;

import android.view.View;
import android.view.ViewGroup;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.GLES20;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.nio.IntBuffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import android.opengl.GLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.opengl.Matrix;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.Rect;

public class DefaultActivity extends Activity {
	private static final String TAG = "Testy.DefaultActivity";

	// private LinearLayout layout;
	// private LinearLayout layoutctl;
	// private SeekBar ctlxs;
	// private SeekBar ctlys;
	// private SeekBar ctlzs;
	private DemoGLView glv;

	public void viewDropParent(View v) {
		ViewGroup p = (ViewGroup) v.getParent();
		if (null != p)
			p.removeView(v);
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout layoutctl = new LinearLayout(this);
		layoutctl.setOrientation(LinearLayout.VERTICAL);
		{
			LinearLayout lctlx = new LinearLayout(this);
			lctlx.setOrientation(LinearLayout.HORIZONTAL);
			SeekBar ctlxs = new SeekBar(this);
			ctlxs.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			ctlxs.setProgress(50);
			ctlxs.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					glv.mvX = ((float) progress / 100.0f - 0.5f) * 30;
					glv.updMatrix();
					glv.requestRender();
				}
				public void onStartTrackingTouch (SeekBar seekBar) { }
				public void onStopTrackingTouch (SeekBar seekBar) { }
			});
			lctlx.addView(ctlxs);
			layoutctl.addView(lctlx);
		}
		{
			LinearLayout lctly = new LinearLayout(this);
			lctly.setOrientation(LinearLayout.HORIZONTAL);
			SeekBar ctlys = new SeekBar(this);
			ctlys.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			ctlys.setProgress(30);
			ctlys.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					glv.mvY = ((float) progress / 100.0f - 0.5f) * 30;
					glv.updMatrix();
					glv.requestRender();
				}
				public void onStartTrackingTouch (SeekBar seekBar) { }
				public void onStopTrackingTouch (SeekBar seekBar) { }
			});
			lctly.addView(ctlys);
			layoutctl.addView(lctly);
		}
		{
			LinearLayout lctlz = new LinearLayout(this);
			lctlz.setOrientation(LinearLayout.HORIZONTAL);
			SeekBar ctlzs = new SeekBar(this);
			ctlzs.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			ctlzs.setProgress(20);
			ctlzs.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					glv.mvZ = ((float) progress / 100.0f - 0.5f) * 30;
					glv.updMatrix();
					glv.requestRender();
				}
				public void onStartTrackingTouch (SeekBar seekBar) { }
				public void onStopTrackingTouch (SeekBar seekBar) { }
			});
			lctlz.addView(ctlzs);
			layoutctl.addView(lctlz);
		}
		layout.addView(layoutctl);
		glv = null;

		try {
			glv = new DemoGLView(this);
			// setContentView(glv);
			layout.addView(glv);
		}
		catch (DemoGLView.MyGLException e) {
			Log.e(TAG, e.toString());
		}
		setContentView(layout);
		// setContentView(R.layout.main);
	}

	@Override
	public void onPause() {
		super.onPause();
		glv.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		glv.onResume();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		System.runFinalizersOnExit(true);
		System.exit(0);
	}
}

class DemoGLView extends GLSurfaceView implements GLSurfaceView.Renderer {
	private static final String TAG = "Testy.DemoGLView";

	private boolean paused = false;
	static private boolean mmReady = false;
	static private FloatBuffer mmVBuf;
	static private FloatBuffer mmTBuf;
	static private ShortBuffer mmFBuf;
	private float[] projmatrix = new float[16];
	private float[] mvmatrix = new float[16];
	private float[] matrix = new float[16];
	private float[] matrix2D = new float[16];
	static private int mmTex = 0;
	static private int mmProg = 0;
	static private int mmUnifmMatrix = -1;
	static private int mmUnifmColor = -1;
	static private int mmUnifmMainTex = -1;
	static private int mmAttribPosition = -1;
	static private int mmAttribTexcoord = -1;

	static private int cProg = 0;
	static private int cUnifmMatrix = -1;
	static private int cUnifmColor = -1;
	static private int cAttribPosition = -1;

	static private int loadingTex = 0;

	public final float scrollLpFactor = 0.6f;
	public float scrollLastTm = 0.0f;
	public float scrollCur = 0.0f;
	public float mmRotX = 90.0f;
	public float mmRotY = 0.0f;
	public float mvX = 0.0f;
	public float mvY = -12.0f;
	public float mvZ = -18.0f;
	public float scaleFactor = 1.0f;
	public float maskSize = 0.0f;
	public long lastframetm = -1;
	public static final float maskIncSpeed = 0.1f;
	public float fps = 0.0f;

	private GestureDetector gestMain;
	private ScaleGestureDetector gestScale;

	private static final String cvshaderSrc =
		"precision mediump float;\n"
		+ "attribute vec3 position;\n"
		+ "uniform mat4 mvpmatrix;\n"
		+ "void main() {\n"
		+ "  gl_Position = mvpmatrix * vec4(position, 1.0);\n"
		+ "}\n";
	private static final String cfshaderSrc =
		"precision mediump float;\n"
		+ "uniform vec4 vColor;\n"
		+ "void main() {\n"
		+ "  gl_FragColor = vColor;\n"
		+ "}\n";
	private static final String vshaderSrc =
		/* "#version 330\n"
		+ "attribute in vec4 position;\n"
		+ "attribute in vec2 texcoord;\n"
		+ "out vec2 vtexcoord;\n"
		+ "void main() {\n"
		+ "  vtexcoord = texcoord;\n"
		+ "  gl_Position = position;\n"
		+ "}\n"; */
		"precision mediump float;\n"
		+ "attribute vec3 position;\n"
		+ "attribute vec2 texcoord;\n"
		+ "uniform mat4 mvpmatrix;\n"
		+ "varying vec2 vtexcoord;\n"
		+ "void main() {\n"
		+ "  vtexcoord = texcoord;\n"
		// + "  gl_Position = vec4(position, 1.0);\n"
		// + "  gl_Position = vec4(position, 1.0) * mvpmatrix;\n"
		+ "  gl_Position = mvpmatrix * vec4(position, 1.0);\n"
		+ "}\n";
	private static final String fshaderSrc =
		/* "#version 330\n"
		+ "out vec4 outputColor;\n"
		+ "in vec2 vtexcoord;\n"
		+ "uniform vec4 vColor;\n"
		+ "uniform sampler2D mainTex;\n"
		+ "void main() {\n"
		+ "  vec3 c = vec3(vColor.r, vColor.g, vColor.b);\n"
		+ "  outputColor = vec4(mix(vec3(texture2D(mainTex, vtexcoord)), c, vColor.a), 1.0f);\n"
		// + "  outputColor = vColor;\n"
		// + "  outputColor = texture2D(mainTex, vtexcoord);\n"
		+ "}\n"; */
		"precision mediump float;\n"
		+ "varying vec2 vtexcoord;\n"
		+ "uniform vec4 vColor;\n"
		+ "uniform sampler2D mainTex;\n"
		+ "void main() {\n"
		+ "  vec4 c = vec4(vec3(vColor), 1.0);\n"
		// + "  gl_FragColor = texture2D(mainTex, vtexcoord);\n"
		// + "  gl_FragColor = vec4(1.0, 0.0, 0.0, 0.5);\n"
		+ "  if (vColor.a > 0.01)\n"
		+ "    gl_FragColor = mix(texture2D(mainTex, vtexcoord), c, vColor.a);\n"
		+ "  else\n"
		+ "    gl_FragColor = texture2D(mainTex, vtexcoord);\n"
		+ "}\n";

	DemoGLView(Context context) throws MyGLException {
		super(context);
		setEGLContextClientVersion(2);
		setEGLConfigChooser(5,6,5,0,16,8);
		setRenderer(this);
		// setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

		// GL calls doesn't work in constructors:
		// http://stackoverflow.com/questions/10600984/android-opengl-glgentextures-not-working-in-constructor
		// The following doesn't work, either.
		/* queueEvent(new Runnable() {
			public void run() {
				try {
					initMetaioman();
				}
				catch (MyGLException e) {
					Log.e(TAG, e.toString());
				}
			}
		}); */

		gestMain = new GestureDetector(getContext(),
				new SimpleOnGestureListener() {
					@Override
					public boolean onScroll(MotionEvent e1, MotionEvent e2,
						float distanceX, float distanceY) {
						Log.d(TAG, "Scroll triggered!" + distanceX);
						// long tm = android.os.SystemClock.uptimeMillis();
						// float factor = 1.0f - (float) Math.pow(1.0f - scrollLpFactor, tm - scrollLastTm);
						// scrollCur = scrollCur * factor + distanceX * (1.0f - factor);
						// rotate(- scrollCur * 0.7f);
						rotate(distanceX * 0.7f, distanceY * 0.7f);
						// scrollLastTm = tm;
						return true;
					}

					@Override
					public boolean onDoubleTap(MotionEvent e) {
						Log.d(TAG, "Double tap triggered!");
						fixedScale();
						return true;
					}
		});
		gestScale = new ScaleGestureDetector(getContext(),
				new SimpleOnScaleGestureListener() {
					@Override
					public boolean onScale(ScaleGestureDetector detector) {
						return scale(detector.getScaleFactor());
					}
				});

		(new AsyncTask<Void, Void, Void>() {
			@Override protected Void doInBackground(Void... v) {
				try {
					initMetaioman();
				}
				catch (MyGLException e) {
					Log.e(TAG, e.toString());
				}
				return null;
			}

			@Override protected void onPostExecute(Void result) {
				mmReady = true;
			}
		}).execute();
	}

	public void rotate(float degX, float degY) {
		float t = mmRotX + degX;
		t = t - (float) Math.floor(t / 360.0f) * 360.0f;
		mmRotX = t;
		mmRotY = Math.min(Math.max(mmRotY - degY, 0.0f), 90.0f);
		updMatrix();
		requestRender();
	}

	public void onPause() {
		resetGL();
		paused = true;
	}

	public void onResume() {
		paused = false;
		requestRender();
	}

	public boolean fixedScale() {
		final float TOLERANCE = 0.2f;
		final float lv1 = 1.0f;
		final float lv2 = 2.0f;

		float diff1 = (float) Math.abs(lv1 - scaleFactor);
		float diff2 = (float) Math.abs(lv2 - scaleFactor);
		if (diff1 <= TOLERANCE)
			scaleFactor = lv2;
		else if (diff2 <= TOLERANCE)
			scaleFactor = lv1;
		else if (diff1 < diff2)
			scaleFactor = lv1;
		else
			scaleFactor = lv2;

		updMatrix();
		requestRender();

		return true;
	}

	public boolean scale(float factor) {
		Log.d(TAG, "SCALE: " + factor);
		scaleFactor *= factor;
		updMatrix();
		requestRender();
		return true;
	}

	private void initMetaioman() throws MyGLException {
		Model mmModel = null;
		try {
			InputStream in = getContext().getAssets().open("metaioman.obj");
			mmModel = Model.fromObj(new InputStreamReader(in), true, false, false);
		}
		catch (IOException e) {
			throw new MyGLException(TAG, "FW " + e.toString());
		}

		// A native-order direct buffer is required
		ByteBuffer bb = null;
		int len = 0;

		len = mmModel.realvertices.length;
		bb = ByteBuffer.allocateDirect(len * 4);
		bb.order(ByteOrder.nativeOrder());
		mmVBuf = bb.asFloatBuffer();
		// mmVBuf = FloatBuffer.allocate(mmModel.realvertices.length);
		mmVBuf.put(mmModel.realvertices);

		len = mmModel.realuv.length;
		bb = ByteBuffer.allocateDirect(len * 4);
		bb.order(ByteOrder.nativeOrder());
		mmTBuf = bb.asFloatBuffer();
		// mmTBuf = FloatBuffer.allocate(mmModel.realuv.length);
		mmTBuf.put(mmModel.realuv);

		// mmFBuf = ShortBuffer.allocate(mmModel.faces.length);
		// mmFBuf.put(mmModel.faces);
	}

	private void initMetaiomanGL() throws MyGLException {
		if (paused) return;

		Bitmap btmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.metaiomantex, null);
		// GLES20.glEnable(GLES20.GL_TEXTURE_2D);
		int tex = createTex2D();
		if (tex <= 0)
			throw new MyGLException("Failed to allocate texture.");

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex);
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, btmap, 0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		mmTex = tex;
		// GLES20.glDisable(GLES20.GL_TEXTURE_2D);
		btmap.recycle();
	}

	@Override
	public void onSurfaceCreated(GL10 unused, EGLConfig config) {
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	}

	public static int loadShader(int type, String code) throws MyGLException {
		int shader = GLES20.glCreateShader(type);
		if (shader >= 0) {
			GLES20.glShaderSource(shader, code);
			GLES20.glCompileShader(shader);
			int[] buf = new int[1];
			buf[0] = GLES20.GL_FALSE;
			GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, buf, 0);
			if (GLES20.GL_TRUE != buf[0]) {
				String log = GLES20.glGetShaderInfoLog(shader);
				throw new MyGLException("Failed to compile shader", log);
			}
		}
		return shader;
	}

	public static int buildProg(int[] shaders) throws MyGLException {
		int prog = GLES20.glCreateProgram();
		if (prog >= 0) {
			for (int i : shaders)
				GLES20.glAttachShader(prog, i);
			GLES20.glLinkProgram(prog);
			int[] buf = new int[1];
			buf[0] = GLES20.GL_FALSE;
			GLES20.glGetProgramiv(prog, GLES20.GL_LINK_STATUS, buf, 0);
			if (GLES20.GL_TRUE != buf[0]) {
				String log = GLES20.glGetProgramInfoLog(prog);
				throw new MyGLException("Failed to link program", log);
			}
		}
		return prog;
	}

	private void drawMetaioman() throws MyGLException {
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		GLES20.glClearDepthf(1.0f);
		GLES20.glDepthFunc( GLES20.GL_LEQUAL );
		GLES20.glDepthMask( true );

		// cull backface
		GLES20.glEnable( GLES20.GL_CULL_FACE );
		GLES20.glCullFace(GLES20.GL_BACK);

		mmVBuf.position(0);
		mmTBuf.position(0);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mmTex);
		GLES20.glUseProgram(mmProg);
		final int vapos = mmAttribPosition;
		final int vtpos = mmAttribTexcoord;
		GLES20.glEnableVertexAttribArray(vapos);
		GLES20.glVertexAttribPointer(vapos, 3,
				GLES20.GL_FLOAT, false, 4 * 3, mmVBuf);
		GLES20.glEnableVertexAttribArray(vtpos);
		GLES20.glVertexAttribPointer(vtpos, 2,
				GLES20.GL_FLOAT, false, 4 * 2, mmTBuf);
		GLES20.glUniform1i(mmUnifmMainTex, 0);
		GLES20.glUniform4f(mmUnifmColor, 0.0f, 0.0f, 1.0f, 0.0f);
		GLES20.glUniformMatrix4fv(mmUnifmMatrix, 1, false, matrix, 0);

		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0,
				mmVBuf.capacity() / 3);

		GLES20.glDisableVertexAttribArray(vapos);
		GLES20.glDisableVertexAttribArray(vtpos);
		GLES20.glUseProgram(0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
	}

	private void drawFrameSub(FloatBuffer frmVBuf, float[] varr) {
		frmVBuf.put(varr);
		frmVBuf.position(0);
		GLES20.glVertexAttribPointer(cAttribPosition, 3,
				GLES20.GL_FLOAT, false, 4 * 3, frmVBuf);
		GLES20.glDrawArrays(GLES20.GL_LINE_STRIP, 0,
				frmVBuf.capacity() / 3);
	}

	private void drawFrame() {
		final float[] pnts = new float[] {
			// Top
			0.916f, 10.536f, -0.10f, 1.0f,
			// Feet
			-0.196f, 0.0f, 1.02f, 1.0f,
			// Feet2
			-0.688f, 0.276f, -1.208f, 1.0f,
			// Cannon
			4.248f, 6.084f, 0.868f, 1.0f,
			// Right arm
			1.292f, 6.105f, 2.593f, 1.0f,
			// Left arm
			0.989f, 5.020f, -3.748f, 1.0f,
		};
		float x1 = 1.0f, x2 = -1.0f, y1 = 1.0f, y2 = -1.0f;
		for (int i = 0; i < pnts.length / 4; ++i) {
			float[] result = new float[4];
			Matrix.multiplyMV(result, 0, matrix, 0, pnts, i * 4);
			for (int j = 0; j < 3; ++j)
				result[j] /= result[3];
			x1 = Math.min(result[0], x1);
			x2 = Math.max(result[0], x2);
			y1 = Math.min(result[1], y1);
			y2 = Math.max(result[1], y2);
		}
		{
			float w = getWidth() / 2.0f, h = getHeight() / 2.0f;
			x1 = Math.max(x1, -1.0f) * w;
			x2 = Math.min(x2, 1.0f) * w;
			y1 = Math.max(y1, -1.0f) * h;
			y2 = Math.min(y2, 1.0f) * h;
		}

		int len = 3 * 3;
		ByteBuffer bb = ByteBuffer.allocateDirect(len * 4);
		bb.order(ByteOrder.nativeOrder());
		FloatBuffer frmVBuf = bb.asFloatBuffer();
		// Log.d(TAG, "Vars: x1 = " + x1 + ", x2 = " + x2 + ", y1 = " + y1 + ", y2 = " + y2);

		GLES20.glUseProgram(cProg);
		GLES20.glUniform4f(cUnifmColor, 0.0f, 0.0f, 0.0f, 1.0f);
		GLES20.glUniformMatrix4fv(cUnifmMatrix, 1, false, matrix2D, 0);
		GLES20.glEnableVertexAttribArray(cAttribPosition);

		GLES20.glLineWidth(3.0f);
		{
			final float lnlen = 10.0f;

			drawFrameSub(frmVBuf, new float[] {
				x1, y1 + lnlen, 1.0f,
				x1, y1, 1.0f,
				x1 + lnlen, y1, 1.0f,
			});
			drawFrameSub(frmVBuf, new float[] {
				x2 - lnlen, y1, 1.0f,
				x2, y1, 1.0f,
				x2, y1 + lnlen, 1.0f,
			});
			drawFrameSub(frmVBuf, new float[] {
				x1, y2 - lnlen, 1.0f,
				x1, y2, 1.0f,
				x1 + lnlen, y2, 1.0f,
			});
			drawFrameSub(frmVBuf, new float[] {
				x2 - lnlen, y2, 1.0f,
				x2, y2, 1.0f,
				x2, y2 - lnlen, 1.0f,
			});
		}

		GLES20.glDisableVertexAttribArray(cAttribPosition);
		GLES20.glUseProgram(0);
	}

	public void drawMask() {
		GLES20.glEnable(GLES20.GL_STENCIL_TEST);
		GLES20.glClear(GLES20.GL_STENCIL_BUFFER_BIT);
		// GLES20.glColorMask(false, false, false, false);
		GLES20.glDepthMask(false);
		// GLES20.glStencilOp(GLES20.GL_REPLACE, GLES20.GL_KEEP, GLES20.GL_KEEP);
		GLES20.glStencilOp(GLES20.GL_KEEP, GLES20.GL_KEEP, GLES20.GL_REPLACE);
		GLES20.glStencilFunc(GLES20.GL_ALWAYS, 0x1, 0x1);

		float[] varr = buildCircle(0.0f, 0.0f, 1.0f, maskSize, 60);
		ByteBuffer bb = ByteBuffer.allocateDirect(varr.length * 4);
		bb.order(ByteOrder.nativeOrder());
		FloatBuffer mskVBuf = bb.asFloatBuffer();
		mskVBuf.put(varr);
		mskVBuf.position(0);

		GLES20.glUseProgram(cProg);
		GLES20.glUniform4f(cUnifmColor, 0.4f, 0.4f, 0.8f, 1.0f);
		GLES20.glUniformMatrix4fv(cUnifmMatrix, 1, false, matrix2D, 0);
		GLES20.glEnableVertexAttribArray(cAttribPosition);
		GLES20.glVertexAttribPointer(cAttribPosition, 3,
				GLES20.GL_FLOAT, false, 4 * 3, mskVBuf);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0,
				mskVBuf.capacity() / 3);
		GLES20.glDisableVertexAttribArray(cAttribPosition);
		GLES20.glUseProgram(0);

		GLES20.glColorMask(true, true, true, true);
		GLES20.glDepthMask(true);
		GLES20.glStencilOp(GLES20.GL_KEEP, GLES20.GL_KEEP, GLES20.GL_KEEP);
		GLES20.glStencilFunc(GLES20.GL_EQUAL, 0x1, 0x1);
		Log.d(TAG, "MASK: " + maskSize);
	}

	public void drawText(String text, float x, float y) {
		TextTex ttex = TextTex.build(text, null, 20.0f);
		if (null == ttex) {
			Log.e(TAG, "Failed to create text texture.");
			return;
		}
		if (x < 0)
			x = getWidth() - ttex.getWidth() + x;
		if (y < 0)
			y = getHeight() - ttex.getHeight() + y;

		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);

		drawTex(ttex.getTex(), ttex.getWidth(), ttex.getHeight(), x, y);

		ttex.close();
		GLES20.glDisable(GLES20.GL_BLEND);
	}

	public void drawTex(int tex, float width, float height, float x, float y) {
		drawTex(tex, width, height, x, y, 0.0f);
	}

	public void drawTex(int tex, float width, float height, float x, float y,
			float rot) {
		x = x - getWidth() / 2.0f;
		y = y - getHeight() / 2.0f;
		FloatBuffer texVBuf = null, texTBuf = null;
		{
			float[] varr = new float[] {
				x, y, 0.0f,
				x + width, y, 0.0f,
				x, y + height, 0.0f,
				x + width, y + height, 0.0f,
			};
			int len = varr.length;
			ByteBuffer bb = ByteBuffer.allocateDirect(len * 4);
			bb.order(ByteOrder.nativeOrder());
			texVBuf = bb.asFloatBuffer();
			texVBuf.put(varr);
			texVBuf.position(0);
		}
		{
			float[] tarr = new float[] {
				0.0f, 1.0f,
				1.0f, 1.0f,
				0.0f, 0.0f,
				1.0f, 0.0f,
			};
			ByteBuffer bb = ByteBuffer.allocateDirect(tarr.length * 4);
			bb.order(ByteOrder.nativeOrder());
			texTBuf = bb.asFloatBuffer();
			texTBuf.put(tarr);
			texTBuf.position(0);
		}

		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex);
		GLES20.glUseProgram(mmProg);
		final int vapos = mmAttribPosition;
		final int vtpos = mmAttribTexcoord;
		GLES20.glEnableVertexAttribArray(vapos);
		GLES20.glVertexAttribPointer(vapos, 3,
				GLES20.GL_FLOAT, false, 4 * 3, texVBuf);
		GLES20.glEnableVertexAttribArray(vtpos);
		GLES20.glVertexAttribPointer(vtpos, 2,
				GLES20.GL_FLOAT, false, 4 * 2, texTBuf);
		GLES20.glUniform1i(mmUnifmMainTex, 0);
		GLES20.glUniform4f(mmUnifmColor, 0.0f, 0.0f, 0.0f, 0.0f);
		// GLES20.glUniform4f(mmUnifmColor, 1.0f, 0.0f, 0.0f, 1.0f);
		{
			float[] m = matrix2D;
			if (0.0 != rot) {
				float[] mvm = new float[16];
				Matrix.setIdentityM(mvm, 0);
				Matrix.rotateM(mvm, 0, rot, 0.0f, 0.0f, 1.0f);
				float[] newm = new float[16];
				Matrix.multiplyMM(newm, 0, m, 0, mvm, 0);
				m = newm;
			}
			GLES20.glUniformMatrix4fv(mmUnifmMatrix, 1, false, m, 0);
		}

		GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0,
				texVBuf.capacity() / 3);

		GLES20.glDisableVertexAttribArray(vapos);
		GLES20.glDisableVertexAttribArray(vtpos);
		GLES20.glUseProgram(0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		long tm = getTime();
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT);

		if (mmReady && lastframetm >= 0) {
			maskSize += (tm - lastframetm) * maskIncSpeed;
		}

		{
			float ratio = (float) Math.pow(0.4, (tm - lastframetm) / 1000.0f);
			fps = fps * ratio + (1000.0f / (tm - lastframetm)) * (1.0f - ratio);
		}

		try {
			if (mmProg <= 0)
				initGL();
			if (mmReady) {
				if (mmTex <= 0)
					initMetaiomanGL();

				float maskMax = 0;
				{
					float w = getWidth() / 2.0f, h = getHeight() / 2.0f;
					maskMax = (float) Math.sqrt(w * w + h * h);
				}
				if (maskSize < maskMax)
					drawMask();
				else {
					GLES20.glClearColor(0.4f, 0.4f, 0.8f, 1.0f);
					GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
				}
				drawMetaioman();
				drawFrame();
				drawText("FPS: " + (int) fps, -15, 15);
				GLES20.glDisable(GLES20.GL_STENCIL_TEST);
			}
			else {
				drawTex(loadingTex, 128, 128, getWidth() / 2.0f - 64, getHeight() / 2.0f - 64, tm / 100 % 30 / 30.0f * 360.0f);
			}
		}
		catch (MyGLException e) {
			Log.e(TAG, e.toString());
			if (null != e.getLog())
				Log.e(TAG, e.getLog());
		}
		lastframetm = tm;
	}

	public long getTime() {
		return android.os.SystemClock.uptimeMillis();
	}

	public static int createTex2D() {
		int tex = 0;
		{
			int[] buf = new int[1];
			GLES20.glGenTextures(1, buf, 0);
			tex = buf[0];
		}
		if (tex <= 0)
			return 0;
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
				GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		return tex;
	}

	public void initGL() throws MyGLException {
		if (paused) return;

		Log.d(TAG, "initGL!");

		GLES20.glClear(GLES20.GL_STENCIL_BUFFER_BIT);
		GLES20.glDisable(GLES20.GL_STENCIL_TEST);
		GLES20.glStencilMask(0x1);
		GLES20.glStencilFunc(GLES20.GL_EQUAL, 0x1, 0x1);

		{
			int[] shaders = new int[2];
			shaders[0] = loadShader(GLES20.GL_VERTEX_SHADER, vshaderSrc);
			shaders[1] = loadShader(GLES20.GL_FRAGMENT_SHADER, fshaderSrc);
			mmProg = buildProg(shaders);
		}

		mmUnifmMatrix = GLES20.glGetUniformLocation(mmProg, "mvpmatrix");
		mmUnifmMainTex = GLES20.glGetUniformLocation(mmProg, "mainTex");
		mmUnifmColor = GLES20.glGetUniformLocation(mmProg, "vColor");
		mmAttribPosition = GLES20.glGetAttribLocation(mmProg, "position");
		mmAttribTexcoord = GLES20.glGetAttribLocation(mmProg, "texcoord");
		/* if (mmUnifmMatrix < 0 || mmUnifmMainTex < 0 || mmUnifmColor < 0
				|| mmAttribPosition < 0 || mmAttribTexcoord < 0)
			throw new MyGLException("Fail to get attribute locations."); */

		{
			int[] shaders = new int[2];
			shaders[0] = loadShader(GLES20.GL_VERTEX_SHADER, cvshaderSrc);
			shaders[1] = loadShader(GLES20.GL_FRAGMENT_SHADER, cfshaderSrc);
			cProg = buildProg(shaders);
		}

		cUnifmMatrix = GLES20.glGetUniformLocation(cProg, "mvpmatrix");
		cUnifmColor = GLES20.glGetUniformLocation(cProg, "vColor");
		cAttribPosition = GLES20.glGetAttribLocation(cProg, "position");
		if (cUnifmMatrix < 0 || cUnifmColor < 0 || cAttribPosition < 0)
			throw new MyGLException("Fail to get attribute locations.");

		{
			Bitmap bm = BitmapFactory.decodeResource(getResources(),
					R.drawable.loading);
			int tex = createTex2D();
			if (tex <= 0)
				throw new MyGLException("Failed to allocate texture.");
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex);
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bm, 0);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
			loadingTex = tex;
		}
	}

	public void resetGL() {
		Log.d(TAG, "resetGL!");
		mmTex = 0;
		mmProg = 0;
		mmUnifmMatrix = -1;
		mmUnifmColor = -1;
		mmUnifmMainTex = -1;
		mmAttribPosition = -1;
		mmAttribTexcoord = -1;

		cProg = 0;
		cUnifmMatrix = -1;
		cUnifmColor = -1;
		cAttribPosition = -1;

		loadingTex = 0;
	}

	public void dumpMatrix(float[] m) {
		if (null == m || 16 != m.length) {
			Log.d(TAG, "dumpMatrix(): Dumping an invalid matrix!");
			return;
		}

		final int SZ = 4;
		for (int i = 0; i < SZ; ++i) {
			String s = "[ ";
			for (int j = 0; j < SZ; ++j) {
				s += m[i * SZ + j];
				if (j < SZ - 1) s += ", ";
			}
			s += " ]";
			Log.d(TAG, "dumpMatrix " + i + ": " + s);
		}
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		GLES20.glViewport(0, 0, width, height);
		updMatrix();

		Matrix.setIdentityM(matrix2D, 0);
		{
			float w = getWidth() / 2.0f, h = getHeight() / 2.0f;
			Matrix.orthoM(matrix2D, 0, - w, w, - h, h, -1000, 1000);
		}
	}

	public void updMatrix() {
		float ratio = (float) getWidth() / getHeight();
		// this projection matrix is applied to object coordinates
		// in the onDrawFrame() method
		Matrix.setIdentityM(mvmatrix, 0);
		// Matrix.translateM(mvmatrix, 0, 0.0f, 0.0f, -0.4f);
		Matrix.translateM(mvmatrix, 0, mvX / scaleFactor, mvY / scaleFactor, mvZ / scaleFactor);
		Matrix.rotateM(mvmatrix, 0, mmRotX, 0.0f, 1.0f, 0.0f);
		Matrix.rotateM(mvmatrix, 0, mmRotY, 0.0f, 0.0f, 1.0f);
		/* Matrix.setLookAtM(mvmatrix, 0,
				mvX, mvY, mvZ,
				// 0.0f, 0.0f, 5.0f,
				0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f); */
		Matrix.setIdentityM(projmatrix, 0);
		Matrix.frustumM(projmatrix, 0, -ratio, ratio, -1, 1, 0.5f, 45f);
		Matrix.multiplyMM(matrix, 0, projmatrix, 0, mvmatrix, 0);
		/* Matrix.setIdentityM(matrix, 0);
		matrix[11] = -0.3f;
		matrix[14] = 2.0f;
		matrix[15] = 10.0f;
		dumpMatrix(matrix); */
		/* Matrix.setIdentityM(matrix, 0);
		matrix[0] = matrix[5] = matrix[10] = 0.15f;
		matrix[7] = -0.5f; */
	}

	public static float[] buildCircle(float centerX, float centerY, float z,
			float radius, int pieces) {
		if (pieces <= 2) return null;
		float[] res = new float[3 * (pieces + 2)];
		res[0] = centerX;
		res[1] = centerY;
		for (int i = 0; i <= pieces; ++i) {
			res[3 * (i + 1)] = centerX
				+ (float) Math.cos(2 * Math.PI / pieces * i) * radius;
			res[3 * (i + 1) + 1] = centerY
				+ (float) Math.sin(2 * Math.PI / pieces * i) * radius;
		}
		for (int i = 0; i < pieces + 2; ++i)
			res[3 * i + 2] = z;
		return res;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		boolean pcd = gestScale.onTouchEvent(ev);
		if (!gestScale.isInProgress())
			pcd = gestMain.onTouchEvent(ev) || pcd;
		return pcd ? true: super.onTouchEvent(ev);
	}

	public static class MyGLException extends Exception {
		private String log;

		protected MyGLException(String msg) {
			super(msg);
		}

		protected MyGLException(String msg, String vlog) {
			super(msg);
			log = vlog;
		}

		public String getLog() { return log; }
	};
}

class Model {
	private static final String TAG = "Testy.Model";
	private static final int NELE_PER_VERT = 4;
	private static final int NELE_PER_TEXCOORD = 3;
	private static final int NELE_PER_NORMAL = 3;

	public static final boolean debug = true;

	static class ModelException extends Exception {
		private ModelException(String message) {
			super(message);
		}

		public static ModelException build(int line, String message) {
			if (line < 0)
				return new ModelException(message);
			return new ModelException(line + ": " + message);
		}
	}

	public float vertices[];
	public float uvw[];
	public float normals[];
	public short faces[];
	public float realvertices[];
	public float realuv[];
	public float realnormals[];

	public static short[] shortArrListToArray(ArrayList<Short> arr) {
		if (null == arr) return null;
		final int len = arr.size();
		short[] result = new short[len];
		for (short i = 0; i < len; ++i)
			result[i] = arr.get(i);
		return result;
	}

	public static float[] fltArrListToArray(ArrayList<Float> arr) {
		if (null == arr) return null;
		final int len = arr.size();
		float[] result = new float[len];
		for (int i = 0; i < len; ++i)
			result[i] = arr.get(i);
		return result;
	}

	public Model(ArrayList<Float> lvertices, ArrayList<Float> luv,
			ArrayList<Float> lnormals, ArrayList<Short> lfaces,
			ArrayList<Float> lrealvertices, ArrayList<Float> lrealuv,
			ArrayList<Float> lrealnormals) {
		vertices = fltArrListToArray(lvertices);
		uvw = fltArrListToArray(luv);
		normals = fltArrListToArray(lnormals);
		faces = shortArrListToArray(lfaces);
		realvertices = fltArrListToArray(lrealvertices);
		realuv = fltArrListToArray(lrealuv);
		realnormals = fltArrListToArray(lrealnormals);
	}

	private void dump() {
		Log.d(TAG, "Dumping model...");
		if (null != vertices)
			Log.d(TAG, "Vertices (" + vertices.length + "):");
		if (null != uvw)
			Log.d(TAG, "UVW (" + uvw.length + "):");
		if (null != normals)
			Log.d(TAG, "Normals (" + normals.length + "):");
		if (null != faces)
			Log.d(TAG, "Faces (" + faces.length + "):");
		if (null != realvertices) {
			Log.d(TAG, "R Vertices (" + realvertices.length + "):");
		}
		if (null != realuv)
			Log.d(TAG, "R UV (" + realuv.length + "):");
		if (null != realnormals)
			Log.d(TAG, "R Normals (" + realnormals.length + "):");
	}

	private static boolean isTkEnd(int ttype) {
		return StreamTokenizer.TT_EOL == ttype
			|| StreamTokenizer.TT_EOF == ttype;
	}

	public static Model fromObj(java.io.Reader in,
			boolean readtexcoord, boolean readnormals, boolean keeporig) {
		ArrayList<Float> vertices = new ArrayList<Float>();
		ArrayList<Float> uvw = new ArrayList<Float>();
		ArrayList<Float> normals = new ArrayList<Float>();
		ArrayList<Short> faces = null;
		if (keeporig)
			faces = new ArrayList<Short>();
		ArrayList<Float> realvertices = new ArrayList<Float>();
		ArrayList<Float> realuv = null;
		ArrayList<Float> realnormals = null;
		if (readtexcoord)
			realuv = new ArrayList<Float>();
		if (readnormals)
			realnormals = new ArrayList<Float>();


		StreamTokenizer tkiz = new StreamTokenizer(in);
		tkiz.commentChar('#');
		tkiz.quoteChar(0);
		tkiz.ordinaryChar('/');
		tkiz.parseNumbers();
		tkiz.lowerCaseMode(true);
		tkiz.eolIsSignificant(true);
		try {
			// One iteration per line
			while (StreamTokenizer.TT_EOF != tkiz.nextToken()) {
				// Empty line
				if (StreamTokenizer.TT_EOL == tkiz.ttype)
					continue;

				// Non-word starts
				if (StreamTokenizer.TT_WORD != tkiz.ttype)
					throw ModelException.build(tkiz.lineno(), "Line not started with "
							+ "a word.");

				// Vertex
				if (tkiz.sval.equals("v")) {
					for (int i = 0; i < NELE_PER_VERT; ++i) {
						tkiz.nextToken();
						if (StreamTokenizer.TT_NUMBER == tkiz.ttype)
							vertices.add((float) tkiz.nval);
						// Default W value is 1.0
						else if (NELE_PER_VERT - 1 == i && isTkEnd(tkiz.ttype)) {
							vertices.add(1.0f);
							tkiz.pushBack();
						}
						else
							throw ModelException.build(tkiz.lineno(), "Invalid token in "
									+ "vertex line.");
					}
				}
				// Texture coordinate (UVW)
				else if (tkiz.sval.equals("vt")) {
					for (int i = 0; i < NELE_PER_TEXCOORD; ++i) {
						tkiz.nextToken();
						if (StreamTokenizer.TT_NUMBER == tkiz.ttype) {
							float v = (float) tkiz.nval;
							if (1 == i) v = 1.0f - v;
							uvw.add(v);
						}
						// Default W value is 0.0
						else if (NELE_PER_TEXCOORD - 1 == i && isTkEnd(tkiz.ttype)) {
							uvw.add(0.0f);
							tkiz.pushBack();
						}
						else
							throw ModelException.build(tkiz.lineno(), "Invalid token in "
									+ "texcoord line.");
					}
				}
				// Normal
				else if (tkiz.sval.equals("vn")) {
					for (int i = 0; i < NELE_PER_NORMAL; ++i) {
						if (StreamTokenizer.TT_NUMBER == tkiz.nextToken())
							normals.add((float) tkiz.nval);
						else
							throw ModelException.build(tkiz.lineno(), "Invalid token in "
									+ "normal line.");
					}
				}
				// Face
				else if (tkiz.sval.equals("f")) {
					// Non-triangles are not supported presently
					for (int i = 0; i < 3; ++i) {
						short[] f = new short[] { -1, -1, -1 };
						final int SZ = f.length;
						for (int j = 0; j < SZ; ++j) {
							// Read number if it exists
							if (StreamTokenizer.TT_NUMBER == tkiz.nextToken())
								f[j] = (short) tkiz.nval;
							else
								tkiz.pushBack();

							// Break out if looks like the element has passed away
							if (StreamTokenizer.TT_NUMBER == tkiz.nextToken()
									|| isTkEnd(tkiz.ttype)) {
								tkiz.pushBack();
								break;
							}
							tkiz.pushBack();

							/* if (!(StreamTokenizer.TT_WORD == tkiz.nextToken()
										&& tkiz.sval.equals("/"))) */
							if ('/' != tkiz.nextToken())
								throw ModelException.build(tkiz.lineno(), "Separator not "
										+ "found in " + i + "-th element.");
						}
						if (f[0] < 0)
							throw ModelException.build(tkiz.lineno(), "Vertex index not found "
									+ "in " + i + "-th element");
						if (keeporig)
							for (short v : f)
								faces.add(v);
						else {
							{
								int idx = (f[0] - 1) * NELE_PER_VERT;
								for (int j = 0; j < NELE_PER_VERT - 1; ++j)
									realvertices.add(vertices.get(idx + j));
							}
							if (null != realuv) {
								// if (uvw.size() > f[1] + 1)
								int idx = (f[1] - 1) * NELE_PER_TEXCOORD;
								for (int j = 0; j < NELE_PER_TEXCOORD - 1; ++j)
									realuv.add(uvw.get(idx + j));
							}
							if (null != realnormals) {
								// if (normals.size() > f[2] + 1)
								int idx = (f[2] - 1) * NELE_PER_NORMAL;
								for (int j = 0; j < NELE_PER_NORMAL; ++j)
									realnormals.add(normals.get(idx + j));
							}
						}
					}
				}
				else if (
						// Parameter space vertex
						tkiz.sval.equals("vp")
						// Material
						|| tkiz.sval.equals("mtllib")
						|| tkiz.sval.equals("usemtl")
						// Named object
						|| tkiz.sval.equals("o")
						// Named group
						|| tkiz.sval.equals("g")
						// Smooth shading
						|| tkiz.sval.equals("s")
						) {
					Log.d(TAG, "Ignored command " + tkiz.sval);
					while (!isTkEnd(tkiz.nextToken()))
						continue;
					tkiz.pushBack();
				}
				else
					throw ModelException.build(tkiz.lineno(), "Unrecognized line.");

				// Make sure it ends now
				if (StreamTokenizer.TT_EOL != tkiz.nextToken()
						&& StreamTokenizer.TT_EOF != tkiz.ttype)
					throw ModelException.build(tkiz.lineno(), "Trailing token.");
			}
		}
		catch (IOException e) {
			Log.e(TAG, e.toString());
			return null;
		}
		catch (ModelException e) {
			Log.e(TAG, e.toString());
			return null;
		}
		if (!keeporig) {
			vertices = null;
			uvw = null;
			normals = null;
			faces = null;
		}
		Model m = new Model(vertices, uvw, normals, faces,
				realvertices, realuv, realnormals);
		m.dump();
		return m;
	}
}

/**
 * A class that generates OpenGL textures containing text.
 */
class TextTex {
	private int tex = 0;
	private int width = 0;
	private int height = 0;

	public int getTex() { return tex; };
	public int getWidth() { return width; };
	public int getHeight() { return height; };

	private TextTex(int tex, int width, int height) {
		this.tex = tex;
		this.width = width;
		this.height = height;
	}

	public void close() {
		if (tex > 0) {
			int[] texs = new int[] { tex };
			GLES20.glDeleteTextures(1, texs, 0);
			tex = 0;
		}
		width = 0;
		height = 0;
	}

	public static TextTex build(String text, Typeface tf, float textsize) {
		Paint p = new Paint();
		if (null == tf) tf = Typeface.MONOSPACE;
		p.setTypeface(tf);
		p.setTextSize(textsize);
		p.setARGB(255, 255, 255, 255);
		p.setAntiAlias(true);
		Rect r = new Rect();
		p.getTextBounds(text, 0, text.length(), r);
		Log.d("Rbuy", "BD: " + r.left + ", " + r.right + ", " + r.top + ", " + r.bottom);
		int w = (int) Math.abs(r.right - r.left);
		int h = (int) Math.abs(r.top - r.bottom);
		Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(bm);
		// c.drawARGB(0, 0, 0, 0);
		// c.drawText(text, 0, 0, p);
		c.drawText(text, - r.left, - r.top, p);
		int tex = DemoGLView.createTex2D();
		if (tex <= 0)
			return null;
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, tex);
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bm, 0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		return new TextTex(tex, w, h);
	}
}
