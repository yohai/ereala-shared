import java.sql.*;
// import com.microsoft.sqlserver.jdbc.*;
import java.util.Dictionary;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.security.SecureRandom;
import java.security.MessageDigest;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.io.IOException;
import org.xml.sax.helpers.XMLReaderFactory;
import org.xml.sax.XMLReader;
import org.xml.sax.SAXException;
import java.util.Hashtable;

/**
 * Master server class.
 *
 * Server for underground cube viewer. Not yet completed.
 */
class Serv {
	static private final boolean DBG_EXCEPTION = false;

	static protected final double RANGE_MAX = 400.0;
	static protected final double RANGE_MIN = 10.0;

	static protected final String RESP_MAP_HEADER = "<map>\n";
	static protected final String RESP_MAP_FOOTER = "</map>\n";
	static protected final String RESP_BADPOS = "...";
	static protected final String RESP_BADSQL = "...";

	static protected final int SESS_EXPIRE = 900;
	static private final String AUTH_SUPERPUB = "1989";
	static protected final int AUTH_PUBLEN = 25;
	static protected final int AUTH_PRIVLEN = 25;
	protected MessageDigest digest;
	static protected final Charset cset = Charset.forName("UTF-8");

	// Must be sorted for binary search!
	static protected final String[] ELES_LINEAR = new String[] { "tube" };

	static private String SQL_CONNSTR =
		"jdbc:sqlserver://your_server.database.windows.net:1433;"
		+ "database=DBNAME;user=USERNAME@SERV;password=PWD";
	private Connection sqlconn;
	static private String SQL_MAPTBLNAME = "map";
	static private String SQL_SESSTBLNAME = "session";
	static private String SQL_USRTBLNAME = "users";
	private PreparedStatement pstmt_auth_dropexpired;
	private PreparedStatement pstmt_auth_addtoken;
	private PreparedStatement pstmt_auth_findtoken;
	private PreparedStatement pstmt_auth_findtoken2;
	private PreparedStatement pstmt_auth_updlastuse;
	private PreparedStatement pstmt_auth_updauth;
	private PreparedStatement pstmt_auth_deauthusr;
	private PreparedStatement pstmt_auth_droptoken;
	private PreparedStatement pstmt_auth_findpwd;
	private PreparedStatement pstmt_auth_addusr;
	private PreparedStatement pstmt_auth_delusr;
	private PreparedStatement pstmt_map_addele;

	private static final Random rand = new SecureRandom();

	// Object initializer
	{
		try {
			digest = MessageDigest.getInstance("SHA256");
		}
		catch (NoSuchAlgorithmException e) {
			// handleException(e);
		}
	}

	/**
	 * Check if a string contains only Base64 characters.
	 */
	public static boolean isBase64(String s) {
		for (int i = 0; i < s.length(); ++i) {
			char c = s.charAt(i);
			if (!(Character.isLetter(c) || Character.isDigit(c)
						|| '+' == c || '/' == c))
				return false;
		}
		return true;
	}

	/**
	 * Check if a byte array contains only Base64 characters.
	 */
	public static boolean isBase64B(byte[] barr) {
		final byte C0 = "0".getBytes(cset)[0];
		final byte C9 = "9".getBytes(cset)[0];
		final byte CA = "A".getBytes(cset)[0];
		final byte CZ = "Z".getBytes(cset)[0];
		final byte Ca = "a".getBytes(cset)[0];
		final byte Cz = "z".getBytes(cset)[0];
		final byte CP = "+".getBytes(cset)[0];
		final byte CS = "/".getBytes(cset)[0];
		for (byte b: barr)
			if (
					!((b >= C0 && b <= C9)
					|| (b >= CA && b <= CZ)
					|| (b >= Ca && b <= Cz)
					|| b == CP || b == CS))
				return false;
		return true;
	}

	/**
	 * Generate a random string of specified length.
	 *
	 * Guaranteed to contain only Base64 characters.
	 */
	protected String genRandStr(int length) {
		if (length <= 0)
			return "";
		byte[] d = new byte[(int) Math.ceil(8.0f * length / 6.0f)];
		rand.nextBytes(d);
		return javax.xml.bind.DatatypeConverter.printBase64Binary(d)
			.substring(0, length);
	}

	/**
	 * Handle an exception.
	 */
	protected void handleException(Exception e) {
		if (DBG_EXCEPTION) {
			System.out.println("Exception encountered: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Validate a public auth token.
	 */
	public static boolean validatePub(String pub) {
		return pub.length() == AUTH_PUBLEN && isBase64(pub);
	}

	/**
	 * Add a user.
	 */
	protected boolean addUser(String username, String password) {
		// Sanity checks
		if (null == username || username.length() <= 0
				|| null == password || password.length() <= 0)
			return false;

		Connection conn = sqlConnect();
		if (null == conn)
			return false;

		if (null == pstmt_auth_addusr) {
			try {
				pstmt_auth_addusr = conn.prepareStatement(
						"INSERT INTO " + SQL_USRTBLNAME
						+ "(username, password)"
						+ " VALUES ( ?, ? );");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null != pstmt_auth_addusr) {
			try {
				pstmt_auth_addusr.setString(1, username);
				pstmt_auth_addusr.setString(2, password);
				if (pstmt_auth_addusr.executeUpdate() >= 0)
					return true;
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		return false;
	}

	/**
	 * Delete a user.
	 */
	protected boolean rmUser(String username) {
		// Sanity checks
		if (null == username || username.length() <= 0)
			return false;

		Connection conn = sqlConnect();
		if (null == conn)
			return false;

		if (null == pstmt_auth_delusr) {
			try {
				pstmt_auth_delusr = conn.prepareStatement(
						"DELETE FROM " + SQL_USRTBLNAME
						+ " WHERE username = ?);");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null != pstmt_auth_delusr) {
			try {
				pstmt_auth_delusr.setString(1, username);
				if (pstmt_auth_delusr.executeUpdate() >= 0)
					return true;
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		return false;
	}

	/**
	 * Generate a set of authentication tokens and insert into database.
	 *
	 * @fixme Handle token conflicts.
	 */
	protected String[] reqAuthToken() {
		Connection conn = sqlConnect();
		if (null == conn)
			return null;

		// Create statement
		if (null == pstmt_auth_addtoken) {
			try {
				pstmt_auth_addtoken = conn.prepareStatement(
						"INSERT INTO " + SQL_SESSTBLNAME
						+ " ( pub, priv, username, tlastuse)"
						+ " VALUES ( ?, ?, \"\", GETDATE() );");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null == pstmt_auth_addtoken)
			return null;

		// Generate tokens
		String[] tokens = new String[2];
		tokens[0] = genRandStr(AUTH_PUBLEN);
		tokens[1] = genRandStr(AUTH_PRIVLEN);

		// Execute statement
		try {
			pstmt_auth_addtoken.setString(1, tokens[0]);
			pstmt_auth_addtoken.setString(2, tokens[1]);
			pstmt_auth_addtoken.executeUpdate();
		}
		catch (SQLException e) {
			handleException(e);
			return null;
		}

		return tokens;
	}

	/**
	 * Concatenate two arrays.
	 */
	public static <T> T[] arrConcat(T[] first, T[] second) {
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}

	public static byte[] arrConcat(byte[] first, byte[] second) {
		byte[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}

	/**
	 * Authorize a token.
	 */
	protected boolean authUsr(String pub, String username, byte[] hash) {
		Connection conn = sqlConnect();
		if (null == conn)
			return false;

		// Prepare statement
		if (null == pstmt_auth_findtoken2) {
			try {
				pstmt_auth_findtoken2 = conn.prepareStatement(
						"SELECT priv, username FROM " + SQL_SESSTBLNAME
						+ " WHERE pub = ?;");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null == pstmt_auth_findtoken2)
			return false;

		byte[] bpriv = null;

		try {
			pstmt_auth_findtoken2.setString(1, pub);
			ResultSet rs = pstmt_auth_findtoken2.executeQuery();
			if (rs.next()) {
				bpriv = rs.getString("priv").getBytes(cset);
				String cusr = rs.getString("username");
				// Already authorized?
				if (null != cusr && cusr.length() >= 0)
					return false;
			}
		}
		catch (SQLException e) {
			handleException(e);
		}

		if (null == bpriv)
			return false;

		if (null == pstmt_auth_findpwd) {
			try {
				pstmt_auth_findpwd = conn.prepareStatement(
						"SELECT password FROM " + SQL_USRTBLNAME
						+ " WHERE username = ?;");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null == pstmt_auth_findpwd)
			return false;

		byte[] bpwd = null;

		try {
			pstmt_auth_findpwd.setString(1, username);
			ResultSet rs = pstmt_auth_findpwd.executeQuery();
			if (rs.next())
				bpwd = rs.getString("password").getBytes(cset);
		}
		catch (SQLException e) {
			handleException(e);
		}

		if (null == bpwd)
			return false;

		boolean result = hashEquals(arrConcat(bpriv, bpwd), hash);

		if (result) {
			if (null == pstmt_auth_updauth) {
				try {
					pstmt_auth_updauth = conn.prepareStatement(
							"UPDATE " + SQL_SESSTBLNAME
							+ " SET username = ?, lastuse = GETDATE()"
							+ " WHERE pub = ?;");
				}
				catch (SQLException e) {
					handleException(e);
				}
			}

			if (null != pstmt_auth_updauth) {
				try {
					pstmt_auth_updauth.setString(1, username);
					pstmt_auth_updauth.setString(2, pub);
					pstmt_auth_updauth.executeUpdate();
				}
				catch (SQLException e) {
					handleException(e);
				}
			}
		}

		return result;
	}

	/**
	 * Deauthorize a token.
	 */
	protected boolean deauthUsr(String pub) {
		Connection conn = sqlConnect();
		if (null == conn)
			return false;

		if (null == pstmt_auth_deauthusr) {
			try {
				pstmt_auth_deauthusr = conn.prepareStatement(
						"UPDATE " + SQL_SESSTBLNAME
						+ "SET username = \"\""
						+ " WHERE pub = ?;");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null == pstmt_auth_deauthusr)
			return false;

		try {
			if (pstmt_auth_deauthusr.executeUpdate() >= 0)
				return true;
		}
		catch (SQLException e) {
			handleException(e);
		}

		return false;
	}

	/**
	 * Drop a token.
	 */
	protected boolean dropToken(String pub) {
		Connection conn = sqlConnect();
		if (null == conn)
			return false;

		if (null == pstmt_auth_droptoken) {
			try {
				pstmt_auth_droptoken = conn.prepareStatement(
						"DELETE FROM " + SQL_SESSTBLNAME
						+ " WHERE pub = ?;");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null == pstmt_auth_droptoken)
			return false;

		try {
			pstmt_auth_droptoken.setString(1, pub);
			if (pstmt_auth_droptoken.executeUpdate() >= 0)
				return true;
		}
		catch (SQLException e) {
			handleException(e);
		}

		return false;
	}

	/**
	 * Check if hash of a byte array is a particular value.
	 */
	protected boolean hashEquals(byte[] contents, byte[] hash) {
		digest.update(contents);
		byte[] dg = digest.digest();
		return Arrays.equals(dg, hash);
	}

	/**
	 * Retrieve username from auth info.
	 *
	 * The hash is the SHA-256 checksum of private token from the server
	 * concatenated by the URL + contents of request.
	 *
	 * @todo Update last use time.
	 */
	protected String authToken(String pub, byte[] hash, byte[] content) {
		if (null == pub || pub.length() != AUTH_PUBLEN
				|| null == hash || hash.length <= 0
				|| null == digest)
			return null;

		Connection conn = sqlConnect();
		if (null == conn)
			return null;

		// Prepare statement
		if (null == pstmt_auth_findtoken) {
			try {
				pstmt_auth_findtoken = conn.prepareStatement(
						"SELECT priv, username, lastuse FROM " + SQL_SESSTBLNAME
						+ " WHERE pub = ?;");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null == pstmt_auth_findtoken)
			return null;

		try {
			pstmt_auth_findtoken.setString(1, pub);
			ResultSet rs = pstmt_auth_findtoken.executeQuery();
			if (rs.next()) {
				byte[] bpriv = rs.getString("priv").getBytes(cset);
				if (hashEquals(arrConcat(bpriv, content), hash)) {
					String result = rs.getString("username");
					if (null != result && result.length() >= 0)
						authUpdLastUse(pub);
					return result;
				}
			}
		}
		catch (SQLException e) {
			handleException(e);
		}

		return null;
	}

	/**
	 * Return an open SQL connection.
	 */
	protected Connection sqlConnect() {
		try {
			if (null == sqlconn || sqlconn.isClosed()) {
				// Ensure the SQL Server driver class is available.
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				// Establish the connection.
				sqlconn = DriverManager.getConnection(SQL_CONNSTR);
			}
		}
		catch (Exception e) {
			handleException(e);
			sqlClose();
		}

		return sqlconn;
	}

	/**
	 * Close SQL connection.
	 */
	protected void sqlClose() {
		if (null != sqlconn) {
			try {
				sqlconn.close();
			}
			catch (Exception e) {
				handleException(e);
			}
		}
		pstmt_auth_dropexpired = null;
		pstmt_auth_addtoken = null;
		pstmt_auth_findtoken = null;
		pstmt_auth_findtoken2 = null;
		pstmt_auth_updlastuse = null;
		pstmt_auth_updauth = null;
		pstmt_auth_deauthusr = null;
		pstmt_auth_droptoken = null;
		pstmt_auth_findpwd = null;
		pstmt_auth_addusr = null;
		pstmt_auth_delusr = null;
		pstmt_map_addele = null;
		sqlconn = null;
	}

	/**
	 * Update last use time of a token.
	 */
	protected void authUpdLastUse(String pub) {
		Connection conn = sqlConnect();
		if (null == conn)
			return;

		if (null == pstmt_auth_updlastuse) {
			try {
				pstmt_auth_updlastuse = conn.prepareStatement(
						"UPDATE " + SQL_SESSTBLNAME
						+ "SET lastuse = GETDATE()"
						+ " WHERE pub = ?;");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null == pstmt_auth_updlastuse)
			return;

		try {
			pstmt_auth_updlastuse.setString(1, pub);
			pstmt_auth_updlastuse.executeUpdate();
		}
		catch (SQLException e) {
			handleException(e);
		}
	}

	/**
	 * Escape a string for placing in XML documents.
	 */
	public static String xmlEscape(String s) {
		return s.replaceAll("&", "&amp;")
			.replaceAll("<", "&lt;")
			.replaceAll(">", "&gt;");
	}

	/**
	 * Process a map request.
	 */
	public String onMapRequest(double latitude, double longitude,
			double range, String[] tags, String username) {
		// Filter out invalid latitude/longitude
		if (Math.abs(latitude) > 90.0 || Math.abs(longitude) > 180.0)
			return RESP_BADPOS;

		// Normalize range
		range = Math.min(Math.max(range, RANGE_MIN), RANGE_MAX);

		Connection conn = sqlConnect();
		if (null == conn)
			return RESP_BADSQL;

		String resp = "";
		{
			// Build SQL statement
			// To check if the circle intersects with the line is probably
			// possible, but I guess a bit too costly in SQL
			String sqlstr = "SELECT cat, la1, lo1, la2, lo2, attrs, tag"
				+ " FROM " + SQL_MAPTBLNAME
				+ " WHERE NOT ( " + (latitude - range) + " >= la2"
				+ " OR " + (latitude + range) + " <= la1"
				+ " OR " + (longitude - range) + " >= lo2"
				+ " OR " + (longitude + range) + " <= lo1 );";
			if (tags.length > 0) {
				sqlstr += " AND ( ";
				for (int i = 0; i < tags.length - 1; ++i)
					sqlstr += "tag = ? OR ";
				sqlstr += "tag = ?";
			}

			ResultSet rs = null;

			try {
				PreparedStatement stmt = conn.prepareStatement(sqlstr);
				for (int i = 0 ; i < tags.length; ++i)
					stmt.setString(i + 1, tags[i]);
				rs = stmt.executeQuery();
			}
			catch (SQLException e) {
				handleException(e);
			}

			// Looping through the results
			if (null == rs)
				return RESP_BADSQL;

			try {
				while (rs.next()) {
					String item = "";
					try {
						// Should use getXX(ID) instead of getXX(name) in the future
						String cat = rs.getString("cat");
						double la1 = rs.getDouble("la1"),
							   lo1 = rs.getDouble("lo1"),
							   la2 = rs.getDouble("la2"),
							   lo2 = rs.getDouble("lo2");
						Dictionary<String, Object> attrs = null;
						{
							Object o = rs.getObject("attrs");
							if (o instanceof Dictionary<?, ?>)
								attrs = (Dictionary<String, Object>) o;
						}
						if (Arrays.binarySearch(ELES_LINEAR, cat) >= 0) {
						}

						// Append to XML response
						item += "<element"
							+ "cat=\"" + xmlEscape(cat) + "\""
							+ " la1=\"" + la1 + "\""
							+ " lo1=\"" + la1 + "\""
							+ " la2=\"" + la2 + "\""
							+ " lo2=\"" + la2 + "\"";
						if (null != attrs) {
							// for (String key : attrs.keys())
							for (Enumeration<String> e = attrs.keys(); e.hasMoreElements(); ) {
								String key = e.nextElement();
								item += " " + key + "=" + xmlEscape(attrs.get(key).toString());
							}
						}
						item += " />";
					}
					catch (SQLException e) {
						handleException(e);
						item = "";
					}
					resp += item;
				}
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		return resp;
	}

	/**
	 * Add a map element.
	 */
	protected boolean addMapEle(MapElement ele) {
		Connection conn = sqlConnect();
		if (null == conn)
			return false;

		if (null == pstmt_map_addele) {
			try {
				pstmt_map_addele = conn.prepareStatement(
						"INSERT INTO " + SQL_MAPTBLNAME
						+ "(category, la1, ll1, la2, ll2, attrs)"
						+ " VALUES ( ?, ?, ?, ?, ? );");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null != pstmt_map_addele) {
			try {
				pstmt_map_addele.setString(1, ele.category);
				pstmt_map_addele.setDouble(2, ele.la1);
				pstmt_map_addele.setDouble(3, ele.ll1);
				pstmt_map_addele.setDouble(4, ele.la2);
				pstmt_map_addele.setDouble(5, ele.ll2);
				pstmt_map_addele.setObject(6, ele.attrs);
				if (pstmt_map_addele.executeUpdate() >= 0)
					return true;
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		return false;
	}

	/**
	 * Parse a XML map element definition.
	 */
	protected MapElement[] parseMapEle(org.xml.sax.InputSource i) {
		try {
			XMLReader parser = XMLReaderFactory.createXMLReader();
			MapElementXmlReader handler = new MapElementXmlReader();
			parser.setContentHandler(handler);
			parser.parse(i);
			return (MapElement[]) handler.eles.toArray();
		}
		catch (SAXException e) {
			handleException(e);
		}
		catch (IOException e) {
			handleException(e);
		}

		return null;
	}

	/**
	 * Cron job.
	 */
	public void onCron() {
		Connection conn = sqlConnect();
		if (null == conn)
			return;

		if (null == pstmt_auth_dropexpired) {
			try {
				pstmt_auth_dropexpired = conn.prepareStatement(
						"DELETE FROM " + SQL_SESSTBLNAME
						+ " WHERE last_active <= (GETDATE() - " + SESS_EXPIRE + ");");
			}
			catch (SQLException e) {
				handleException(e);
			}
		}

		if (null != pstmt_auth_dropexpired) {
			try {
				pstmt_auth_dropexpired.executeUpdate();
			}
			catch (SQLException e) {
				handleException(e);
			}
		}
	}

	/**
	 * Driver.
	 */
	public static int Main(String[] args) {
		return 0;
	}
}

/**
 * Representation of a map element.
 */
class MapElement {
	public String category;
	public double la1, ll1, la2, ll2;
	public Dictionary<String, Object> attrs;

	MapElement() {
		attrs = new Hashtable<String, Object>();
	}

	MapElement(String vcat) {
		this();
		category = vcat;
	}

	MapElement(String vcat,
			double vla1, double vll1, double vla2, double vll2,
			Dictionary<String, Object> vattrs) {
		category = vcat;
		la1 = vla1;
		ll1 = vll1;
		la2 = vla2;
		ll2 = vll2;
		attrs = vattrs;
	}

	public void addAttr(String key, Object val) {
		if (null == attrs)
			attrs = new Hashtable<String, Object>();
		attrs.put(key, val);
	}
}

class MapElementXmlReader extends org.xml.sax.helpers.DefaultHandler {
	private MapElement cur;
	public List<MapElement> eles = new ArrayList<MapElement>();

	public void startElement(String uri, String localName,
			String qName, org.xml.sax.Attributes atts) throws SAXException {
		if (localName.equals("elementlist"))
			return;
		if (null != cur)
			throw new SAXException("An element started inside another.");
		String cat = localName;
		cur = new MapElement(cat);
		for (int i = 0; i < atts.getLength(); ++i) {
			String n = atts.getLocalName(i);
			String v = atts.getValue(i);
			try {
				if (n.equals("la1"))
					cur.la1 = Double.parseDouble(v);
				else if (n.equals("ll1"))
					cur.ll1 = Double.parseDouble(v);
				else if (n.equals("la2"))
					cur.la2 = Double.parseDouble(v);
				else if (n.equals("ll2"))
					cur.ll2 = Double.parseDouble(v);
				else
					cur.addAttr(n, v);
			}
			catch (NumberFormatException e) {
				throw new SAXException("I wanted a number but there's none!");
			}
		}
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (localName.equals("elementlist"))
			return;
		if (null == cur)
			throw new SAXException("End of a non-existent element?");
		if (!cur.category.equals(localName))
			throw new SAXException("Nested element end?");
		eles.add(cur);
	}
}
