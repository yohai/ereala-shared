﻿using UnityEngine;
using System.Collections;

public class SAnimSlider : MonoBehaviour {
	public Animation anim;
	public bool vertical = false;
	public Rect posProportional = new Rect(0.0f, 1.0f, 0.3f, 0.0f);
	public Rect posOffset = new Rect(150.0f, -40.0f, 0.0f, 20.0f);
	public GUISkin skin;
	public string anim_name;
	public bool pauseOnMove = false;
	public bool playOnMove = false;

	// Use this for initialization
	void Start () {
		if (!anim)
			anim = gameObject.animation;
	}
	
	// Update is called once per frame
	void Update() {
	}

	void OnGUI() {
		string name = anim_name;
		if (string.IsNullOrEmpty(anim_name))
			name = anim.clip.name;
		AnimationState state = anim[name];
		float curval = state.time;
		GUI.skin = skin;
		Rect pos = SGUIUtils.MapScreenRect(posProportional, posOffset);
		float newval = (!vertical ?
				GUI.HorizontalSlider(pos, curval, 0.0f, state.length):
				GUI.VerticalSlider(pos, curval, 0.0f, state.length));
		if (Mathf.Abs(curval - newval) > 0.1f) {
			bool orig_enabled = state.enabled;
			state.enabled = true;
			state.speed = 1.0f;
			state.weight = 1.0f;
			anim.Rewind(name);
			state.time = newval;
			anim.Sample();
			if (pauseOnMove)
				state.enabled = false;
			else if (playOnMove)
				state.enabled = true;
			else
				state.enabled = orig_enabled;
		}
	}
}
