﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class metaioHelper : MonoBehaviour {
	public metaioSDK sdk;
	public GUISkin skin;
	[HideInInspector]
	public Color invalidBackgroundColor = new Color(1.0f, 0.0f, 0.0f);

	private List<float[]> lastTrackingValues;
	private List<float[]> rememberedTrackingValues;

	public int numberOfCOSs = 1;

	void Start() {
	}

	float TextFieldFloat(string label, float val, ref string sval, ref bool invalid_mark, float min = 0.0f, float max = float.PositiveInfinity) {
		float result = 0;

		GUILayout.BeginVertical();
		GUILayout.Label(label);
		{
			Color bg_old = GUI.backgroundColor;
			if (invalid_mark)
				GUI.backgroundColor = invalidBackgroundColor;
			sval = GUILayout.TextField(sval);
			invalid_mark = !(float.TryParse(sval, out result)
					&& result >= min && result <= max);
			if (invalid_mark)
				result = val;
			GUI.backgroundColor = bg_old;
		}
		GUILayout.EndVertical();

		return result;
	}

	void UpdateTransformArray(ref List<float[]> arr) {
		if (null == arr)
			arr = new List<float[]>(numberOfCOSs);
		int num_diff = arr.Count - numberOfCOSs;
		if (num_diff > 0)
			arr.RemoveRange(numberOfCOSs, num_diff);
		else if (num_diff < 0)
			for (int i = 0; i < - num_diff; ++i)
				arr.Add(null);
		for (int i = 0; i < numberOfCOSs; ++i)
			if (null == arr[i])
				arr[i] = new float[7];

	}

	void OnGUI() {
		if (skin)
			GUI.skin = skin;

		const float right = 20.0f;
		const float top = 40.0f;
		const float itemHeight = 25.0f;
		const float itemWidth = 70.0f;
		const float padding = 10.0f;
		const float cosPadding = 10.0f;
		const string floatFormatSpec = "g";

		UpdateTransformArray(ref lastTrackingValues);
		UpdateTransformArray(ref rememberedTrackingValues);

		float[] tracking_vals = new float[7];
		for (int i = 1; i < numberOfCOSs + 1; ++i) {
			Rect bound = new Rect(Screen.width - right - (numberOfCOSs - i + 1) * (itemWidth + padding * 2.0f + cosPadding) + cosPadding, top, itemWidth, itemHeight * 18);
			GUI.Box(new Rect(bound.x - padding,
						bound.y - padding,
						bound.width + padding * 2.0f,
						bound.height + padding * 2.0f), "");
			float quality = metaioSDK.getTrackingValues(i, tracking_vals);
			if (quality <= 0.0f)
				tracking_vals = lastTrackingValues[i - 1];
			GUILayout.BeginArea(bound);
			GUILayout.BeginVertical();

			GUILayout.Toggle((quality > 0.0f), "COS " + i.ToString());

			{
				Vector3 p = new Vector3(tracking_vals[0], tracking_vals[1], tracking_vals[2]);
				Quaternion q = new Quaternion(tracking_vals[3], tracking_vals[4], tracking_vals[5], tracking_vals[6]);
				Vector3 ea = q.eulerAngles;

				GUILayout.Label("Position:");
				GUILayout.TextField(p.x.ToString(floatFormatSpec));
				GUILayout.TextField(p.y.ToString(floatFormatSpec));
				GUILayout.TextField(p.z.ToString(floatFormatSpec));
				GUILayout.Label("Rotation:");
				GUILayout.TextField(ea.x.ToString(floatFormatSpec));
				GUILayout.TextField(ea.y.ToString(floatFormatSpec));
				GUILayout.TextField(ea.z.ToString(floatFormatSpec));
			}

			if (GUILayout.Button("Save"))
				rememberedTrackingValues[i - 1] = (float[]) lastTrackingValues[i - 1].Clone();

			{
				float[] remembered_vals = rememberedTrackingValues[i - 1];
				Vector3 p = new Vector3(remembered_vals[0], remembered_vals[1], remembered_vals[2]);
				Quaternion q = new Quaternion(remembered_vals[3], remembered_vals[4], remembered_vals[5], remembered_vals[6]);
				Vector3 ea = q.eulerAngles;

				GUILayout.Label("Position:");
				GUILayout.TextField(p.x.ToString(floatFormatSpec));
				GUILayout.TextField(p.y.ToString(floatFormatSpec));
				GUILayout.TextField(p.z.ToString(floatFormatSpec));
				GUILayout.Label("Rotation:");
				GUILayout.TextField(ea.x.ToString(floatFormatSpec));
				GUILayout.TextField(ea.y.ToString(floatFormatSpec));
				GUILayout.TextField(ea.z.ToString(floatFormatSpec));
			}

			GUILayout.EndVertical();
			GUILayout.EndArea();

			lastTrackingValues[i - 1] = (float[]) tracking_vals.Clone();
		}

		// TODO
		if (sdk && GUI.Button(new Rect(Screen.width - 120.0f, Screen.height - 40.0f, 100.0f, 20.0f), "Reload")) {
			metaioSDK.setTrackingConfigurationFromAssets(sdk.trackingConfiguration);
		}
	}
}
