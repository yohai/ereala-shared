﻿using UnityEngine;
using System.Collections;

public abstract class SMoldButtonBase : MonoBehaviour {
	protected bool mouse_in = false;

	void OnMouseEnterMoldButton() {
		mouse_in = true;
		Debug.Log(gameObject.name + ": Mouse enter");
	}

	void OnMouseLeaveMoldButton() {
		mouse_in = false;
		Debug.Log(gameObject.name + ": Mouse leave");
	}

	void OnMouseOverMoldButton() {
		// Debug.Log(gameObject.name + ": Mouse over");
	}

	void OnMouseClickMoldButton() {
		Debug.Log(gameObject.name + ": Mouse click");
	}
}
