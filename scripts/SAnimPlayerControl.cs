﻿using UnityEngine;
using System.Collections;

public class SAnimPlayerControl : MonoBehaviour {
	public GUISkin skin;
	public Animation anim;
	public AnimationClip clip;
	public Vector2 archorProportional = new Vector2(0.0f, 1.0f);
	public Vector2 archorOffset = new Vector2(20.0f, -60.0f);
	public float buttonWidth = 50.0f;
	public float buttonHeight = 50.0f;
	public float buttonSpacing = 20.0f;
	public GUIContent playButton = new GUIContent("Play");
	public GUIContent pauseButton = new GUIContent("Pause");
	public GUIContent stopButton = new GUIContent("Stop");

	// Use this for initialization
	void Start () {
		if (!anim)
			anim = gameObject.animation;
	}

	// Update is called once per frame
	void Update () {
	}

	void Play() {
		string name = GetName();
		anim.Play(name);
		anim[name].enabled = true;
	}

	void Pause() {
		string name = GetName();
		anim[name].enabled = false;
	}

	void Rewind() {
		string name = GetName();
		anim[name].time = 0.0f;
		anim.Sample();
	}

	void Stop() {
		string name = GetName();
		anim[name].enabled = true;
		Rewind();
		anim.Stop(name);
	}

	bool IsPlaying() {
		string name = GetName();
		return anim[name] && anim[name].enabled && anim[name].speed > 0.0f;
	}

	void OnGUI() {
		GUI.skin = skin;
		Vector2 archor =
			SGUIUtils.MapScreenV2(archorProportional, archorOffset);
		float cur_x = archor.x;
		if (!IsPlaying()) {
			if (GUI.Button(new Rect(cur_x, archor.y, buttonWidth, buttonHeight),
					playButton))
				Play();
		}
		else {
			if (GUI.Button(new Rect(cur_x, archor.y, buttonWidth, buttonHeight),
					pauseButton))
				Pause();
		}
		cur_x += buttonWidth + buttonSpacing;

		if (GUI.Button(new Rect(cur_x, archor.y, buttonWidth, buttonHeight),
				stopButton))
			Stop();
	}

	string GetName() {
		return (clip ? clip.name: anim.clip.name);
	}
}
