using UnityEngine;
using System.Collections;
using System;

///
/// <summary>
/// Display a 2D pop-up plane + lines on click.</summary>
/// <remarks>
/// Enable "Word wrap" or "Text clipping" in textstyle if you find it failing.
/// </remarks>
public class SPopcorn : MonoBehaviour {
	// public int layer = 0;
	public S2DCamera cam;
	public Vector3 lineoffset = new Vector3(0.0f, 0.0f, 1.0f);
	public Vector3 planeoffset = new Vector3(0.0f, 0.0f, 1.0f);
	public Vector2 textoffset = new Vector2(0.0f, 0.0f);
	public Vector2 textsz = new Vector2(0.0f, 0.0f);
	public string text = "";
	public GUIStyle textstyle;
	public LineRenderer tpl_popline;
	public Transform tpl_popplane;
	public Vector3[] route = new Vector3[1];
	protected LineRenderer[] poplines;
	protected Transform popplane;
	private Vector3 DEFPOS = new Vector3(0.0f, 0.0f, 1.0f);
	private bool ongoing = false;
	private int cidx = 0;
	private Vector3 posc = new Vector3(0, 0, 0);
	// private Vector3 POSSTART = new Vector3(0.0f, 0.0f, 0.0f);
	public float step = 10.0f;
	public bool removeOnClickSelf = false;
	public bool removeOnClickElsewhere = false;
	public float removeDelay = -1.0f;
	protected float triggerTm = -1.0f;
	public Vector3[] removeOnOutScrPos;
	public float removeOnOutScrDelay = 0.0f;
	protected float removeOnOutScrLostTm = -1.0f;

	// Use this for initialization
	void Start() {
	}

	public static Vector3 V3SimpleProduct(Vector3 a, Vector3 b) {
		return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
	}

	public static bool PtIsInScr(Camera cam, Vector3 pt) {
		Vector3 vpt = cam.WorldToViewportPoint(pt);
		print(pt + " -> " + vpt);
		return vpt.x >= 0.0f && vpt.x <= 1.0f && vpt.y >= 0.0f && vpt.y <= 1.0f
			&& vpt.z >= cam.nearClipPlane && vpt.z <= cam.farClipPlane;
		// cam.projectMatrix * cam.worldToCameraMatrix.Multiply
	}

	// Update is called once per frame
	void Update() {
		if (null == popplane) return;

		if (triggerTm >= 0.0f && removeDelay >= 0.0f
				&& Time.time >= triggerTm + removeDelay) {
			reset();
			return;
		}

		if (popplane.gameObject.activeInHierarchy) {
			if ((removeOnClickSelf || removeOnClickElsewhere)
					&& Input.GetMouseButtonDown(0)) {
				RaycastHit hit;
				bool clicked = popplane.gameObject.collider.Raycast(
						cam.gameObject.camera.ScreenPointToRay(Input.mousePosition),
						out hit, 100.0f);
				print("Raycasting..." + clicked);
				if ((removeOnClickSelf && clicked)
						|| (removeOnClickElsewhere && !clicked)) {
					reset();
					return;
				}
			}

			if (null != removeOnOutScrPos && removeOnOutScrPos.Length > 0) {
				bool inscreen = false;
				foreach (Vector3 v in removeOnOutScrPos)
					if (PtIsInScr(cam.gameObject.camera,
								popplane.localToWorldMatrix.MultiplyPoint(v))) {
						inscreen = true;
						print("In screen!");
						break;
					}
				if (inscreen)
					removeOnOutScrLostTm = -1.0f;
				else if (removeOnOutScrLostTm < 0.0f)
					removeOnOutScrLostTm = Time.time;
				print(removeOnOutScrLostTm);
				if (!inscreen
						&& Time.time > removeOnOutScrLostTm + removeOnOutScrDelay) {
					reset();
					return;
				}
			}
		}

		{
			Vector3 scrpos = Camera.main.WorldToScreenPoint(
					transform.localToWorldMatrix.MultiplyPoint(lineoffset));
			Vector3 lpos = new Vector3(scrpos.x - Screen.width / 2.0f,
					scrpos.y - Screen.height / 2.0f, 1.0f);
			if (null != poplines)
				foreach (LineRenderer l in poplines)
					if (l)
						l.gameObject.transform.localPosition = lpos + lineoffset;
			Vector3 endpos = lpos;
			if (route.Length > 0)
				endpos += route[route.Length - 1];
			popplane.localPosition = endpos + planeoffset;
		}

		if (!ongoing) return;

		if (route.Length > 0) {
			int cidx_old = cidx;
			float stepc = step * Time.deltaTime;
			// Vector3 prev = (cidx > 0 ? route[cidx]: POSSTART);
			Vector3 next = route[cidx];
			while ((next - posc).magnitude <= stepc) {
				// popline.SetPosition(cidx, next);
				// poplines[cidx].transform.SetPosition(1, next);
				stepc -= (next - posc).magnitude;
				posc = route[cidx];
				++cidx;
				if (cidx > route.Length - 1)
					break;
				// prev = route[cidx - 1];
				next = route[cidx];
			}
			if (cidx != cidx_old) {
				// popline.SetVertexCount(Math.Min(cidx + 1, route.Length));
				for (; cidx_old <= Math.Min(cidx, poplines.Length - 1); ++cidx_old) {
					if (!poplines[cidx_old])
						poplines[cidx_old] = CreatePopLine(route[cidx_old - 1]);
					poplines[cidx_old].SetPosition(1, route[cidx_old]);
				}
			}
			if (cidx < route.Length) {
				posc += (stepc / (next - posc).magnitude) * (next - posc);
				poplines[cidx].SetPosition(1, posc);
				/* for (int i = cidx; i < route.Length; ++i)
					popline.SetPosition(i, posc); */
			}
		}
		if (cidx >= route.Length) {
			ongoing = false;
			popplane.gameObject.SetActive(true);
		}
	}

	public void OnMouseDown() {
		print("Mouse down!");
		if (null != popplane) return;
		CreatePop();
		ongoing = true;
	}

	protected void CreatePop() {
		triggerTm = Time.time;
		if (null == poplines && route.Length > 0) {
			poplines = new LineRenderer[route.Length];
			poplines[0] = CreatePopLine(new Vector3(0.0f, 0.0f, 0.0f));
			/*
			popline = (LineRenderer) Instantiate(tpl_popline,
					DEFPOS, Quaternion.identity);
			popline.gameObject.transform.parent = cam.transform;
			popline.SetVertexCount(1);
			popline.SetPosition(0, new Vector3(0.0f, 0.0f, 0.0f));
			*/
		}
		if (!popplane) {
			Vector3 pos = DEFPOS;
			if (route.Length > 0)
				pos += route[route.Length - 1];
			popplane = (Transform) Instantiate(tpl_popplane,
					pos, Quaternion.identity);
			popplane.parent = cam.transform;
			popplane.gameObject.SetActive(false);
		}
	}

	protected void reset() {
		if (null != poplines) {
			foreach (LineRenderer r in poplines)
				Destroy(r.gameObject);
			poplines = null;
		}
		if (null != popplane) {
			Destroy(popplane.gameObject);
			popplane = null;
		}
		cidx = 0;
		posc = new Vector3(0.0f, 0.0f, 0.0f);
		ongoing = false;
		triggerTm = -1.0f;
		removeOnOutScrLostTm = -1.0f;
	}

	private LineRenderer CreatePopLine(Vector3 pos) {
		LineRenderer popline = (LineRenderer) Instantiate(tpl_popline,
				DEFPOS, Quaternion.identity);
		popline.gameObject.transform.parent = cam.transform;
		popline.SetVertexCount(2);
		popline.SetPosition(0, pos);
		popline.SetPosition(1, pos);
		return popline;
	}

	void OnGUI() {
		if (popplane && popplane.gameObject.activeInHierarchy) {
			Vector2 planepos = new Vector2(popplane.localPosition.x + Screen.width / 2.0f, popplane.localPosition.y + Screen.height / 2.0f);
			Vector2 ptstart = planepos + textoffset;
			GUI.Label(new Rect(ptstart.x, Screen.height - ptstart.y, textsz.x, textsz.y), text, textstyle);
		}
	}
}
