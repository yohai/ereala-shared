// #define CFG_GESTURE

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

/// <summary> 
/// metaioTracker subclass that contains a lot of weird features.</summary>
public class metaioTrackerPersistent : metaioTracker {
	// true if childs' rendering is enabled, else false
	private bool childsEnabled2;

	public bool persistent = false;
	public metaioTrackerPersistent[] trackersToWatch;
	public float persistent_animtime = 0.0f;
	public Vector3 persistent_pos = new Vector3(0.0f, 0.0f, 0.0f);
	public Vector3 persistent_rot = new Vector3(0.0f, 0.0f, 0.0f);
#if CFG_GESTURE
	public TapRecognizer rectap;
	public PinchRecognizer recpinch;
	public TwistRecognizer rectwist;
	private float pinchstart;
#endif

	public bool protect_anim = false;
	private bool tracked = false;
	public bool Tracked {
		get { return tracked; }
	}

	public float rotlp_factor = 0.0f;
	private Vector3 rotlp_cur;

	private float ps_tmleft = 0.0f;
	private Vector3 lastpos;
	private Quaternion lastrot;
	private float tmlast = 0.0f;
	private bool once_tracked = false;
	private bool last_tracked = false;
	private bool inPersistent = false;
	public bool InPersistent {
		get { return inPersistent; }
	}
	public float ps_scale = 1.0f;
	private Quaternion ps_rot = Quaternion.identity;
	public Vector3 ps_rotaxis;

	public class AnimationStateData {
		public float time;
		public AnimationStateData(AnimationState s) {
			time = s.time;
		}
	};
	public string test = "";
	public bool debugPose = false;

	public bool movableY = false;
	public float movableYOffset = 0.0f;
	private float movableYCur = 0.0f;

	public bool detectOrientation = false;

	// private Dictionary<Animation, AnimationStateData> animdata;
	// private ArrayList<KeyValuePair<Animation, AnimationStateData>> animdata;
	private ArrayList animdata;

	void Awake()
	{
		// find the MainCamera, it should have the metaioCamera script attached
		cameraToPosition = Camera.main;
		if(tranformCamera && ! cameraToPosition)
		{
			 Debug.LogError("there is no cameraToPosition set");
		}

		childsEnabled2 = true;
	}

	void Start() {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		// trackersToWatch = GetComponentsInSiblings<metaioTrackerPersistent>();
#if CFG_GESTURE
		if (rectap)
			rectap.OnGesture += OnTap;
		if (recpinch) {
			test = "Awesome 1";
			recpinch.OnGesture += OnPinch;
		}
		if (rectwist)
			rectwist.OnGesture += OnTwist;
#endif
	}

	// Update is called once per frame
	void Update() {
		if (simulatePose) {
			// use this predefines pose
			Quaternion q = new Quaternion(-0.2f, -0.8f, 0.4f, 0.4f);
			Vector3 p = new Vector3(14.7f, 12.8f, 203.0f);
			SetPos(p, q);
			return;
		}

		float[] trackingValues = new float[7];
		float quality = metaioSDK.getTrackingValues(cosID, trackingValues);
		tracked = (quality > 0.0f);
		if (tracked != last_tracked && once_tracked)
			ps_tmleft = persistent_animtime;

		inPersistent = false;
		bool processed = false;
		if (tracked) {
			once_tracked = true;

			Vector3 p = new Vector3(trackingValues[0], trackingValues[1], trackingValues[2]);
			Quaternion q = new Quaternion(trackingValues[3], trackingValues[4], trackingValues[5], trackingValues[6]);

			if (detectOrientation) {
				Vector3 rotv = q.eulerAngles;
				switch (Screen.orientation) {
					case ScreenOrientation.Portrait:
						break;
					case ScreenOrientation.LandscapeLeft:
						break;
					case ScreenOrientation.LandscapeRight:
						break;
					case ScreenOrientation.PortraitUpsideDown:
						break;
				}
				q = Quaternion.Euler(rotv);
			}

			if (movableY) {
				Vector3 rotv = q.eulerAngles;
				if (!last_tracked)
					movableYCur = rotv.y + movableYOffset;
				Debug.Log("OFFSETY: " + movableYCur);
				rotv.y -= movableYCur;
				q = Quaternion.Euler(rotv);
			}

			SetPos(p, q);

			// show childs
			enableRenderingChilds(true);
			processed = true;
		}
		else if (persistent && once_tracked) {
			bool others_tracked = false;
			foreach (metaioTrackerPersistent tr in trackersToWatch)
				if (tr.Tracked) {
					others_tracked = true;
					break;
				}

			if (!others_tracked) {
				Vector3 pos = persistent_pos / ps_scale;
				Quaternion rot = Quaternion.Euler(persistent_rot) * ps_rot;
				if (ps_tmleft > 0.0f) {
					float step = Time.time - tmlast;
					// if (null != lastpos)
						pos = lastpos + (pos - lastpos) * step / ps_tmleft;
					// if (null != lastrot)
						rot = Quaternion.Slerp(lastrot, rot, step / ps_tmleft);
					ps_tmleft -= step;
					print(step);
				}
				SetPos(pos, rot);

				// show children
				enableRenderingChilds(true);
				processed = true;
				inPersistent = true;
			}
		}
		if (!processed) {
			// hide because target not tracked
			enableRenderingChilds(false);
		}

		tmlast = Time.time;
		last_tracked = tracked;
	}

	public static Vector3 V3Clone(Vector3 src) {
		return new Vector3(src.x, src.y, src.z);
	}

	protected void SetPos(Vector3 p, Quaternion q) {
		// Low-pass filter
		if (rotlp_factor > 0.0f) {
			// rotlp_cur = Vector3.Lerp(qv - lastrot, lastrot, rotlp_factor);
			q = Quaternion.Lerp(q, lastrot,
					Mathf.Pow(rotlp_factor, Time.time - tmlast));
		}

		lastpos = p;
		lastrot = q;

		if (!tranformCamera) {
			transform.position = p;
			transform.rotation = q;
		}
		else {
			setCameraPosition(p, q);
		}
	}

	protected void setCameraPosition(Vector3 p, Quaternion q) {
		// todo, make a function out of this, otherwhise its the same as metaioTracker.cs
		Matrix4x4 rotationMatrix = new Matrix4x4();
		NormalizeQuaternion(ref q);

		rotationMatrix.SetTRS(Vector3.zero,
		                       q,
		                       new Vector3(1.0f, 1.0f, 1.0f));

		Matrix4x4 translationMatrix = new Matrix4x4();
		translationMatrix.SetTRS(p,
		                       new Quaternion(0.0f, 0.0f, 0.0f, 1.0f),
		                       new Vector3(1.0f, 1.0f, 1.0f));

		//split up rotation and translation
		Matrix4x4 composed = translationMatrix * rotationMatrix;
		//from world to camera so we have to invert the matrix
		Matrix4x4 invertedMatrix = composed.inverse;

        //center the camera in front of goal - z-axis
		cameraToPosition.transform.position = invertedMatrix.GetColumn(3);
		cameraToPosition.transform.rotation = QuaternionFromMatrix(invertedMatrix);
	}

	private void TransformFromMatrix(Matrix4x4 matrix, Transform trans) {
	    trans.rotation = QuaternionFromMatrix(matrix);
	    trans.position = matrix.GetColumn(3); // uses implicit conversion from Vector4 to Vector3
	}

	private Quaternion QuaternionFromMatrix(Matrix4x4 m) {
	    // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm

	    Quaternion q = new Quaternion();
	    q.w = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] + m[1,1] + m[2,2] ) ) / 2;
	    q.x = Mathf.Sqrt( Mathf.Max( 0, 1 + m[0,0] - m[1,1] - m[2,2] ) ) / 2;
	    q.y = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] + m[1,1] - m[2,2] ) ) / 2;
	    q.z = Mathf.Sqrt( Mathf.Max( 0, 1 - m[0,0] - m[1,1] + m[2,2] ) ) / 2;

	    q.x *= Mathf.Sign( q.x * ( m[2,1] - m[1,2] ) );
	    q.y *= Mathf.Sign( q.y * ( m[0,2] - m[2,0] ) );
	    q.z *= Mathf.Sign( q.z * ( m[1,0] - m[0,1] ) );

	    return q;

	}

	private void NormalizeQuaternion (ref Quaternion q)
	{
	    float sum = 0;
	    for (int i = 0; i < 4; ++i)
	        sum += q[i] * q[i];
	    float magnitudeInverse = 1 / Mathf.Sqrt(sum);
	    for (int i = 0; i < 4; ++i)
	        q[i] *= magnitudeInverse;
	}

	// Enable/disable rendering
	private void enableRenderingChilds(bool enable) {
		// Do nothing if enabled state is not changed
		if (childsEnabled2 == enable)
			return;

		Transform wrapper = transform.Find("metaioTrackerWrapper");
		if (null != wrapper) {
			if (!enable)
				ProtectAnim();
			wrapper.gameObject.SetActive(enable);
			if (enable)
				RestoreAnim();
		}
		else {
			Renderer[] rendererComponents =
				GetComponentsInChildren<Renderer>();

			foreach (Renderer component in rendererComponents) {
				component.enabled = enable;
			}
		}

		childsEnabled2 = enable;
    }

	protected void ProtectAnim() {
		if (protect_anim) {
			if (null == animdata)
				animdata = new ArrayList();
			foreach (Animation a in GetComponentsInChildren<Animation>()) {
				AnimationState s = a[a.clip.name];
				if (null != s && s.enabled)
					animdata.Add(new KeyValuePair<Animation, AnimationStateData>(a, new AnimationStateData(s)));
			}
		}
	}

	protected void RestoreAnim() {
		if (null != animdata) {
			foreach (KeyValuePair<Animation, AnimationStateData> p in animdata) {
				Animation a = p.Key;
				a[a.clip.name].enabled = true;
				a[a.clip.name].time = p.Value.time;
			}
			animdata = null;
		}
	}

	public static T[] GetComponentsInSiblings<T>(Transform t) where T : Component {
		List<T> arr = new List<T>();
		foreach (Transform c in t.parent) {
			T[] eles = c.GetComponents<T>();
			if (null != eles)
				arr.AddRange(eles);
		}
		return arr.ToArray();
	}

	public T[] GetComponentsInSiblings<T>() where T : Component {
		return GetComponentsInSiblings<T>(transform);
	}

#if CFG_GESTURE
	void OnTap(TapGesture g) {
		Debug.Log("Tapped!");
	}

	void OnPinch(PinchGesture g) {
		if (!inPersistent) return;
		ps_scale *= g.Gap / (g.Gap - g.Delta);
		test += "\nScaling " + ps_scale;
		Debug.Log("Scaling " + ps_scale);
	}

	void OnTwist(TwistGesture g) {
		if (!inPersistent) return;
		ps_rot *= Quaternion.AngleAxis(g.DeltaRotation, ps_rotaxis);
		test = "\nRotated!";
	}
#endif

	void OnGUI() {
		if (debugPose) {
			float[] trackingValues = new float[7];
			float quality = metaioSDK.getTrackingValues(cosID,
					trackingValues);
			string pstr = "None";
			if (quality > 0.0f) {
				Vector3 p = new Vector3(trackingValues[0], trackingValues[1], trackingValues[2]);
				Quaternion q = new Quaternion(trackingValues[3], trackingValues[4], trackingValues[5], trackingValues[6]);
				Vector3 rotv = q.eulerAngles;
				pstr = "Q: " + quality + ", P: (" + p.x + ", " + p.y + ", " + p.z + "),\n"
					+ "Q: (" + q.x + ", " + q.y + ", " + q.z + ", " + q.w + "),\n"
					+ "QV: (" + rotv.x + ", " + rotv.y + ", " + rotv.z + ")\n";
			}
			if (movableY)
				pstr += " (+ " + movableYCur + " Y)";
			GUI.TextArea(new Rect(10, 10, Screen.width - 10, 50), pstr);
		}

		if (test.Length > 0)
			GUI.TextField(new Rect(Screen.width - 200, Screen.height - 50, 200, 50), test);
	}
}
