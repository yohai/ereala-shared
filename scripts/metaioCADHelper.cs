﻿using UnityEngine;
using System.Collections;

public class metaioCADHelper : MonoBehaviour {
	public enum SensorUsage { Off, Gravity, All };
	[HideInInspector]
	public static string[] SensorUsageStrings = { "off", "gravity", "all" };

	public metaioSDK sdk;
	public GUISkin skin;
	public Color invalidBackgroundColor = new Color(1.0f, 0.0f, 0.0f);
	public float padding = 10.0f;
	// public float textfieldWidth = 30.0f;
	public float translateStep = 1.0f;
	public float rotateStep = 10.0f;

	private bool ready = false;
	public int numberOfCOSs = 1;

	public uint numFeatures = 100;
	private string numFeaturesText = "";
	private bool invalidNumFeatures = false;
	public float searchRange = 50.0f;
	private string searchRangeText = "";
	private bool invalidSearchRange = false;
	public float minQuality = 0.7f;
	private string minQualityText = "";
	private bool invalidMinQuality = false;
	public bool visiblityTest = false;
	public float visibilityTestRate = 0.2f;
	private string visibilityTestRateText = "";
	private bool invalidVisibilityTestRate = false;
	public SensorUsage sensorUsage = SensorUsage.Off;

	void Start() {
	}

	void UpdateVisiblityTest() {
		metaioSDK.sensorCommand("setVisibilityTestEnabled",
				(visiblityTest ? "on": "off"));
	}

	void UpdateNumFeatures() {
		metaioSDK.sensorCommand("setNumFeatures", numFeatures.ToString());
	}

	void UpdateSearchRange() {
		metaioSDK.sensorCommand("setSearchRange", searchRange.ToString());
	}

	void UpdateMinQuality() {
		metaioSDK.sensorCommand("setMinQuality", minQuality.ToString());
	}

	void UpdateVisibilityTestRate() {
		metaioSDK.sensorCommand("setVisibilityTestRate", visibilityTestRate.ToString());
	}

	void UpdateSensorUsage() {
		metaioSDK.sensorCommand("setSensorUsage",
				SensorUsageStrings[(int) sensorUsage]);
	}

	void InitializeParameters() {
		UpdateVisiblityTest();
		UpdateNumFeatures();
		numFeaturesText = numFeatures.ToString();
		UpdateSearchRange();
		searchRangeText = searchRange.ToString();
		UpdateMinQuality();
		minQualityText = minQuality.ToString();
		UpdateVisibilityTestRate();
		visibilityTestRateText = visibilityTestRate.ToString();
		UpdateSensorUsage();

		metaioSDK.setRendererClippingPlaneLimits(Camera.main.nearClipPlane, Camera.main.farClipPlane);
		metaioCamera.updateCameraProjectionMatrix();

		ready = true;
	}

	float TextFieldFloat(string label, float val, ref string sval, ref bool invalid_mark, float min = 0.0f, float max = float.PositiveInfinity) {
		float result = 0;

		GUILayout.BeginVertical();
		GUILayout.Label(label);
		{
			Color bg_old = GUI.backgroundColor;
			if (invalid_mark)
				GUI.backgroundColor = invalidBackgroundColor;
			sval = GUILayout.TextField(sval);
			invalid_mark = !(float.TryParse(sval, out result)
					&& result >= min && result <= max);
			if (invalid_mark)
				result = val;
			GUI.backgroundColor = bg_old;
		}
		GUILayout.EndVertical();

		return result;
	}

	void OnGUI() {
		if (skin)
			GUI.skin = skin;

		if (numberOfCOSs > 0) {
			const float itemHeight = 16.0f;
			Rect bound = new Rect(20.0f, Screen.height - 20.0f - numberOfCOSs * itemHeight, 60.0f, numberOfCOSs * itemHeight);
			GUI.Box(new Rect(bound.x - padding, bound.y - padding, bound.width + padding * 2.0f, bound.height + padding * 2.0f), "");
			float[] tracking_vals = new float[7];
			for (int i = 1; i < numberOfCOSs + 1; ++i) {
				float quality =
					metaioSDK.getTrackingValues(i, tracking_vals);
				GUI.Toggle(new Rect(bound.x, bound.y + (i - 1) * itemHeight, bound.width, itemHeight), (quality > 0.0f), "COS " + i.ToString());
			}
		}

		if (!ready) {
			if (GUI.Button(new Rect(Screen.width - 120.0f, 20.0f, 100.0f, 20.0f), "Initialize"))
				InitializeParameters();
			return;
		}

		Rect area = new Rect(Screen.width - 120.0f, 20.0f, 100.0f, 500.0f);
		GUI.Box(new Rect(area.x - padding, area.y - padding, area.width + 2 * padding, area.height + 2 * padding), "");
		GUILayout.BeginArea(area);
		GUILayout.BeginVertical();

		{
			SensorUsage new_sensor_usage = (SensorUsage)
				GUILayout.SelectionGrid((int) sensorUsage, SensorUsageStrings, 1);
			if (new_sensor_usage != sensorUsage) {
				sensorUsage = new_sensor_usage;
				UpdateSensorUsage();
			}
		}

		{
			uint result = (uint) TextFieldFloat("NumFeatures", numFeatures, ref numFeaturesText, ref invalidNumFeatures);
			if (result != numFeatures) {
				numFeatures = result;
				UpdateNumFeatures();
			}
		}

		/*
		GUILayout.BeginVertical();
		GUILayout.Label("Num Features");
		{
			Color bg_old = GUI.backgroundColor;
			if (invalidNumFeatures)
				GUI.backgroundColor = invalidBackgroundColor;
			int result = 0;
			invalidNumFeatures = (int.TryParse(GUILayout.TextField(numFeaturesText), out result) && result > 0);
			if (!invalidNumFeatures) {
				numFeatures = (uint) result;
				UpdateNumFeatures();
			}
			GUI.backgroundColor = bg_old;
		}
		GUILayout.EndVertical();
		*/

		{
			float result = TextFieldFloat("SearchRange", searchRange, ref searchRangeText, ref invalidSearchRange);
			if (result != searchRange) {
				searchRange = result;
				UpdateSearchRange();
			}
		}

		{
			float result = TextFieldFloat("MinQuality", minQuality, ref minQualityText, ref invalidMinQuality, 0.0f, 1.0f);
			if (result != minQuality) {
				minQuality = result;
				UpdateMinQuality();
			}
		}

		if (visiblityTest !=
				GUILayout.Toggle(visiblityTest, "VisiblityTest")) {
			visiblityTest = !visiblityTest;
			UpdateVisiblityTest();
		}

		{
			float result = TextFieldFloat("VisiblityTestRate", visibilityTestRate, ref visibilityTestRateText, ref invalidVisibilityTestRate, 0.0f, 1.0f);
			if (result != visibilityTestRate) {
				visibilityTestRate = result;
				UpdateVisibilityTestRate();
			}
		}

		GUILayout.BeginHorizontal();
		GUILayout.Label("Pos:");
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label("X");
		if (GUILayout.Button("+"))
			metaioSDK.sensorCommand("translatePose", translateStep + " " + 0.0f);
		if (GUILayout.Button("-"))
			metaioSDK.sensorCommand("translatePose", (- translateStep) + " " + 0.0f);
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Y");
		if (GUILayout.Button("+"))
			metaioSDK.sensorCommand("translatePose", 0.0f + " " + translateStep);
		if (GUILayout.Button("-"))
			metaioSDK.sensorCommand("translatePose", 0.0f + " " + (- translateStep));
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Rot:");
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.Label("X");
		if (GUILayout.Button("+"))
			metaioSDK.sensorCommand("rotatePose", rotateStep + " " + 0.0f);
		if (GUILayout.Button("-"))
			metaioSDK.sensorCommand("rotatePose", (- rotateStep) + " " + 0.0f);
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUILayout.Label("Y");
		if (GUILayout.Button("+"))
			metaioSDK.sensorCommand("rotatePose", 0.0f + " " + rotateStep);
		if (GUILayout.Button("-"))
			metaioSDK.sensorCommand("rotatePose", 0.0f + " " + (- rotateStep));
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();

		if (sdk && GUILayout.Button("Reload")) {
			metaioSDK.setTrackingConfigurationFromAssets(sdk.trackingConfiguration);
			ready = false;
		}

		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
}
