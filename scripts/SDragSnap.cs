using UnityEngine;
using System.Collections;

/// <summary>
/// Drag an object and snap to particular points/objects.</summary>
public class SDragSnap : MonoBehaviour {
	public float postolerance = 100.0f;
	public float rottolerance = 180.0f;
	public Camera cam;
	public Transform[] objs;
	public Vector3[] ofsts;
	public Quaternion[] rots;
	private float camdist = 0.0f;
	private Vector3 dragoffset;

	void Start() {
		if (null == cam)
			cam = Camera.main;
	}

	void OnMouseDown() {
		print("Mouse down!");
		camdist = 0.0f;
		dragoffset = new Vector3(0.0f, 0.0f, 0.0f);
		RaycastHit hit;
		if (!collider.Raycast(cam.ScreenPointToRay(Input.mousePosition),
					out hit, 1000.0f)) {
			print("Failed to hit anything with Raycast. It's an unlucky day today!");
			return;
		}
		dragoffset = transform.position - hit.point;
		camdist = hit.distance;
		print("Distance = " + camdist + ", offset = " + dragoffset);
	}

	void OnMouseDrag() {
		print("Mouse drag!");
		if (!FindSnap(Input.mousePosition)) {
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			transform.position = ray.GetPoint(camdist) + dragoffset;
		}
	}

	public bool FindSnap(Vector2 mpos) {
		float closestd = float.MaxValue;
		Vector3 closestv = new Vector3(0.0f, 0.0f, 0.0f);
		int closesti = 0;
		for (int i = 0; i < ofsts.Length; ++i) {
			Vector3 p = GetPos(objs[i], ofsts[i]);
			Vector3 pc = cam.WorldToScreenPoint(p);
			float d = (V3ToV2(pc) - mpos).magnitude;
			print("Close to element " + i + ": distance = " + d + ", p = " + p + ", rot = " + Quaternion.Angle(transform.rotation, rots[i]));
			if (d < postolerance && d < closestd
					&& (rottolerance >= 360.0f || Mathf.Abs(Quaternion.Angle(transform.rotation, rots[i])) < rottolerance)) {
				print("Best snap!");
				closestd = d;
				closestv = p;
				closesti = i;
			}
		}
		if (closestd >= float.MaxValue)
			return false;
		transform.position = closestv;
		if (rottolerance < 360.0f)
			transform.rotation = rots[closesti];
		return true;
	}

	public static Vector2 V3ToV2(Vector3 v) {
		return new Vector2(v.x, v.y);
	}

	public static Vector3 V3SimpleProduct(Vector3 a, Vector3 b) {
		return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
	}

	public Vector3 GetPos(Transform obj, Vector3 offset) {
			if (null != obj)
				return obj.position
					+ V3SimpleProduct(obj.rotation * offset, obj.lossyScale);
			return offset;
	}
}
