using UnityEngine;
using System.Collections;

/// <summary> 
/// Trigger animation on click and flip object states afterwards.</summary> 
public class SClickAnim : MonoBehaviour {
	public Animation[] tgts = null;
	public MeshRenderer[] meshflip;
	public Transform[] gobjflip;
	private int cidx = -1;

	/*
	// Use this for initialization
	void Start () {
	} */

	// Update is called once per frame
	void Update () {
		/*
			foreach (AnimationState tgtst in tgts[cidx]) {
				print(tgtst.time + " " + tgtst.length);
			} */
		if (cidx >= 0 && !tgts[cidx].isPlaying) {
			// print("Triggered" + cidx);
			++cidx;
			if (cidx >= tgts.Length) {
				foreach (MeshRenderer m in meshflip) {
					if (null != m)
						m.enabled = !m.enabled;
				}
				foreach (Transform t in gobjflip) {
					GameObject g = t.gameObject;
					g.SetActive(!g.activeSelf);
				}
				cidx = 0;
				enabled = false;
			}
			else {
				tgts[cidx].Play();
			}
		}
			/*
			foreach (AnimationState tgtst in tgts[cidx]) {
				print(tgtst.time + " " + tgtst.length);
				if (tgtst.time >= tgtst.length * 0.95f) {
					print("Triggered!");
					enabled = false;
				}
			}
			*/
	}

	void OnMouseDown() {
		if (cidx < 0) {
			cidx = 0;
			tgts[cidx].Play();
			// print("Mod " + cidx);
		}
	}

	// Coutesy of http://answers.unity3d.com/questions/37411/how-can-i-wait-for-an-animation-to-complete.html
	public static IEnumerator WaitForAnimation(Animation animation)
	{
		do {
			yield return null;
		} while (animation.isPlaying);
	}
}
