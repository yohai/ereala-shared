﻿using UnityEngine;
using System.Collections;

public class SShowHideAnim : MonoBehaviour {
	public Animation anim = null;
	public AnimationClip clip = null;

	public float[] ruleTime = null;
	public Transform[] ruleObjects = null;
	public MeshRenderer[] ruleMeshRenderers = null;
	public bool[] ruleEnable = null;

	public struct Rule {
		public bool enableRule;
		public float time;
		public Transform t;
		public MeshRenderer mr;
		public bool enable;
	};
	public Rule[] rules;

	// Use this for initialization
	void Start () {
		if (null == rules) {
			rules = new Rule[ruleTime.Length];
			for (int i = 0; i < rules.Length; ++i) {
				Rule r = new Rule();
				r.enableRule = true;
				r.time = ruleTime[i];
				if (ruleObjects.Length > i)
					r.t = ruleObjects[i];
				if (ruleMeshRenderers.Length > i && !r.t)
					r.mr = ruleMeshRenderers[i];
				r.enable = ruleEnable[i];
				rules[i] = r;
			}
		}
	}
	
	// Update is called once per frame
	void Update() {
		Animation cur_anim = anim;
		if (!cur_anim) cur_anim = animation;
		AnimationClip cur_clip = clip;
		if (!cur_clip) cur_clip = cur_anim.clip;
		float time = cur_anim[cur_clip.name].time;

		foreach (Rule r in rules) {
			if (!r.enableRule || r.time > time)
				continue;
			if (r.t)
				r.t.gameObject.SetActive(r.enable);
			else
				r.mr.enabled = r.enable;
		}
	}
}
