using UnityEngine;
using System.Collections;

/// <summary> 
/// Show/hide elements after a specific period of time.</summary>
public class SShowHide : MonoBehaviour {
	public float[] chtimes;
	public Transform[] exgobj;
	public float[] exgobjtm;

	// public bool recursive = false;
	public float rotatetime = 0.0f;
	private int lastpass = 0;
	public class Rule {
		public float time;
		public Transform t;
		public MeshRenderer mr;
		public bool triggered;
	};
	public Rule[] rules;


	// Use this for initialization
	void Start () {
		rules = new Rule[chtimes.Length + exgobj.Length];
		for (int i = 0; i < rules.Length; ++i)
			rules[i] = new Rule();
		int bs = 0;
		for (int i = bs; i < chtimes.Length; ++i) {
			rules[i].time = chtimes[i];
			rules[i].mr = GetComponent<MeshRenderer>();
		}
		bs += chtimes.Length;
		for (int i = bs; i < exgobj.Length + bs; ++i) {
			rules[i].time = exgobjtm[i];
			rules[i].t = exgobj[i];
		}
	}

	/* void invert() {
		{
			MeshRenderer mr = GetComponent<MeshRenderer>();
			if (null != mr)
				mr.enabled = !mr.enabled;
		}
		if (recursive)
			foreach (Transform t in transform) {
				GameObject o = t.gameObject;
				o.SetActive(!o.activeSelf);
			}
		foreach (Transform t in exgobj) {
			GameObject o = t.gameObject;
			o.SetActive(!o.activeSelf);
		}
	} */

	void Update() {
		float tm = Time.time;
		if (rotatetime > 0.0f) {
			int pass = Mathf.FloorToInt(tm / rotatetime);
			if (pass != lastpass) {
				lastpass = pass;
				foreach (Rule r in rules)
					r.triggered = false;
			}
			tm -= pass * rotatetime;
		}
		foreach (Rule r in rules) {
			if (!r.triggered && tm >= r.time) {
				if (r.t) {
					GameObject o = r.t.gameObject;
					o.SetActive(!o.activeSelf);
				}
				else if (r.mr) {
					r.mr.enabled = !r.mr.enabled;
				}
				r.triggered = true;
			}
		}
	}
}
