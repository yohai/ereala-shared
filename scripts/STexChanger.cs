using UnityEngine;
using System.Collections;

/// <summary>
/// Change texture with simple GUI.</summary>
public class STexChanger : MonoBehaviour {
	public Texture[] texs;
	public string[] names;
	public float butwid = 128.0f;
	public float buthei = 32.0f;

	void Start() {
	}

	void Update() {
	}

	public bool ChangeTex(int id) {
		if (null != texs && texs.Length > id) {
			renderer.enabled = (null == texs[id]);
			renderer.material.mainTexture = texs[id];
			return true;
		}
		return false;
	}

	public bool ChangeTex(string name) {
		if (null == names) return false;
		for (int i = 0; i < names.Length; ++i)
			if (names[i] == name) {
				return ChangeTex(i);
			}
		return false;
	}

	void OnGUI() {
		if (null == texs) return;
		const float sptop = 15.0f;
		const float spright = 15.0f;
		const float spwid = 10.0f;
		float curx = Screen.width - spright;
		float cury = sptop;
		for (int i = 0; i < texs.Length; ++i) {
			string name = i.ToString();
			if (null != names && i < names.Length)
				name = names[i];
			if (GUI.Button(new Rect(curx - butwid, cury, butwid, buthei), name))
				ChangeTex(i);
			curx -= spwid + butwid;
		}
	}
}
