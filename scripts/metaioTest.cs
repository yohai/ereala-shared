﻿using UnityEngine;
using System.Collections;

public class metaioTest : MonoBehaviour {

	int Fetch3DPointsFromMap(string map_path, string save_to) {
		float[] pnts = new float[10000];
		int ret = metaioSDK.get3DPointsFrom3Dmap (map_path, pnts);
		if (ret > 0) {
			Debug.Log (ret + " points found");
			System.IO.File.AppendAllText(save_to, "// " + map_path + "\n");
			for (int i = 0; i < ret; ++i)
				System.IO.File.AppendAllText(save_to, "(" + pnts[3 * i] + ", " + pnts[3 * i + 1] + ", " + pnts[3 * i + 2] + "),\n");
			System.IO.File.AppendAllText(save_to, "// END\n");
		}
		return ret;
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnGUI() {
		if (GUI.Button(new Rect (10.0f, 10.0f, 100.0f, 100.0f), "Fetch3DPoints"))
				Fetch3DPointsFromMap(Application.streamingAssetsPath + "/ml3d/testmap.3dmap", Application.streamingAssetsPath + "/ml3d/out.txt");
	}
}
