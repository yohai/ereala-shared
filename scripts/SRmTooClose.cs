using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Stop drawing all mesh when camera is too close.</summary>
public class SRmTooClose : MonoBehaviour {
	public Camera cam;
	public Vector3[] monitorPts;
	public bool cubePts = false;
	public static bool PtIsTooClose(Camera cam, Vector3 pt, float threshold) {
		Vector3 vpt = cam.WorldToViewportPoint(pt);
		return vpt.z < threshold;
	}
	public float threshold = 0.0f;

	// Use this for initialization
	void Start () {
		if (null == cam)
			cam = Camera.main;
		if (threshold <= 0.0f)
			threshold = cam.nearClipPlane;
		if (cubePts) {
			Vector3[] pts = new Vector3[monitorPts.Length + 8];
			Array.Copy(monitorPts, pts, monitorPts.Length);
			int c = monitorPts.Length;
			float[] eles = new float[] { -0.5f, 0.5f };
			foreach (float i in eles)
				foreach (float j in eles)
					foreach (float k in eles)
						pts[c++] = new Vector3(i, j, k);
			monitorPts = pts;
		}
	}
	
	// Update is called once per frame
	void Update () {
		// bool tooclose = false;
		foreach (Vector3 v in monitorPts)
			if (PtIsTooClose(cam.gameObject.camera,
						transform.localToWorldMatrix.MultiplyPoint(v), threshold)) {
				renderer.enabled = false;
				return;
			}
		renderer.enabled = true;
	}
}
