using UnityEngine;
using System.Collections;

public class SRestrictMove : MonoBehaviour {
	public Vector3 start;
	public Vector3 end;

	// Use this for initialization
	void Start() {
	}

	// Update is called once per frame
	void Update() {
	}

	void FixedUpdate() {
		transform.position = Vector3.Min(Vector3.Max(transform.position, start), end);
	}
}
