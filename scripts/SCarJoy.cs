using UnityEngine;
using System.Collections;

/// <summary>
/// Control a car with Easy Joystick-s.</summary>
public class SCarJoy : MonoBehaviour {
	public EasyJoystick jstkSpeed;
	public EasyJoystick jstkRot;
	public Rigidbody tgtr;
	public float force = 1000000f;
	public float tforce = 0.001f;
	public float brakefactor = 0.95f;

	// Use this for initialization
	void Start () {
		/*
		EasyJoystick.On_JoystickMove += On_JoystickMove;
		EasyJoystick.On_JoystickMoveStart += On_JoystickMoveStart;
		EasyJoystick.On_JoystickMoveEnd += On_JoystickMoveEnd;
		*/
	}

	// Update is called once per frame
	void FixedUpdate () {
		const float yanglebase = 0.0f;
		Transform tgtt = tgtr.transform;
		float angle = (tgtt.rotation.eulerAngles.y - yanglebase)
			* Mathf.Deg2Rad;
		tgtr.AddForce((new Vector3(
						Mathf.Sin(angle), 0.0f, Mathf.Cos(angle)))
				* jstkSpeed.JoystickValue.y);
		if (jstkSpeed.JoystickAxis.y < 0.0f)
			tgtr.velocity *= Mathf.Pow(brakefactor, - jstkSpeed.JoystickAxis.y);
		if (0.0f != jstkRot.JoystickAxis.y) {
			Vector3 ev = tgtt.rotation.eulerAngles;
			float yd = (Mathf.Atan(Mathf.Abs(jstkRot.JoystickAxis.x) / jstkRot.JoystickAxis.y) * Mathf.Rad2Deg);
			yd = Mathf.Clamp(yd * tforce * tgtr.velocity.magnitude, -90.0f, 90.0f);
			ev = new Vector3(ev.x, ev.y + yd, ev.z);
			tgtt.rotation = Quaternion.Euler(ev);
		}
	}

	/*
	void On_JoystickMove(MovingJoystick move){
	}

	void On_JoystickMoveEnd(MovingJoystick move) {
	}

	void On_JoystickMoveStart(MovingJoystick move) {
	}
	*/
}
