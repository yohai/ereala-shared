using UnityEngine;
using System.Collections;

/// <summary>
/// Scroll an object based on tracking target rotation.</summary>
public class SScroller : MonoBehaviour {
	public float tgtwidth = 200.0f;
	public float tgtheight = 200.0f;
	public metaioTracker tracker = null;

	public Vector3 stdp;
	public Quaternion stdq;
	public float basewid = 200.0f;
	public float basehei = 200.0f;
	public Transform wrapper;
	public Transform tfmtgt;
	public float tfmspeed = 4.0f;

	private Vector3 p;
	private Quaternion q;
	private float tmlast;

	// Use this for initialization
	void Start () {
		FindTracker();
		if (!wrapper)
			wrapper = transform;
		/* wrapper.localPosition = new Vector3(- tgtwidth / 2.0f,
				0.0f, - tgtheight / 2.0f); */
		wrapper.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
		wrapper.localScale = new Vector3(tgtwidth, 1.0f, tgtheight);
	}
	
	// Update is called once per frame
	void Update () {
		if (tfmtgt && GetTrackingVals()) {
			Vector3 tp = tfmtgt.localPosition;
			Vector3 e = q.eulerAngles;
			float speed = tfmspeed * (Time.time - tmlast);
			tp.x += Mathf.Sin((90.0f - e.z) * Mathf.Deg2Rad) * speed;
			tp.z += Mathf.Sin((e.y - 270.0f) * Mathf.Deg2Rad) * speed;
			tp.x = Mathf.Clamp(tp.x, -0.5f, 0.5f);
			tp.y = Mathf.Clamp(tp.y, -0.5f, 0.5f);
			tp.z = Mathf.Clamp(tp.z, -0.5f, 0.5f);
			tfmtgt.localPosition = tp;
		}
		tmlast = Time.time;
	}

	public void FindTracker() {
		if (null == tracker) {
			Transform t = transform;
			while (null == t.gameObject.GetComponent<metaioTracker>()) {
				if (null == (t = t.parent))
					throw new System.ApplicationException("Traveled all the way to root and still no tracker!");
			}
			tracker = t.gameObject.GetComponent<metaioTracker>();
		}
	}

	public bool GetTrackingVals() {
		if (null == tracker)
			return false;

		int cosID = tracker.cosID;
		float[] trackingValues = new float[7];
		bool tracked = (metaioSDK.getTrackingValues(cosID,
					trackingValues) > 0.0f);
		if (!tracked)
			return false;

		p.x = trackingValues[0];
		p.y = trackingValues[1];
		p.z = trackingValues[2];

		q.x = trackingValues[3];
		q.y = trackingValues[4];
		q.z = trackingValues[5];
		q.w = trackingValues[6];

		return true;
	}
}
