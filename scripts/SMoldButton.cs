﻿using UnityEngine;
using System.Collections;

public class SMoldButton : MonoBehaviour {
	public SMoldMainCamera mainCamera;
	public Color normalColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
	public Color maxColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	public float hoverWhitishSpeed = 0.0f;
	public bool clickWhitish = true;
	// public Transform test;
	protected bool mouse_in = false;
	public GUISkin skin;
	public string description;
	public Rect descriptionGeometry =
		new Rect(-20.0f, -20.0f, 300.0f, 50.0f);
	public bool exitOnClick = false;
	public Transform switchToOnClick = null;
	public Transform switchToOnClickGroup = null;
	public bool switchToOnClickRewindAnim = false;
	public bool switchToOnClickPlayAnim = false;
	public bool switchToOnClickDraggable = false;
	public Behaviour[] switchToOnClickEnableBehaviours;
	public Behaviour[] switchToOnClickDisableBehaviours;
	public GUITexture changeBgTarget;
	public Texture changeBgTo;

	// Use this for initialization
	void Start() {
	}

	// Update is called once per frame
	void Update() {
		Color o = guiTexture.color;
		Color c = o;
		float delta = hoverWhitishSpeed * Time.deltaTime;
		if (clickWhitish && mouse_in && Input.GetMouseButton(0)) {
			c = maxColor;
		}
		else if (hoverWhitishSpeed > 0.0f) {
			if (mouse_in) {
				c = new Color(
						Mathf.Min(o.r + delta, maxColor.r),
						Mathf.Min(o.g + delta, maxColor.g),
						Mathf.Min(o.b + delta, maxColor.b),
						Mathf.Min(o.a + delta, maxColor.a));
			}
			else {
				c = new Color(
						Mathf.Max(o.r - delta, normalColor.r),
						Mathf.Max(o.g - delta, normalColor.g),
						Mathf.Max(o.b - delta, normalColor.b),
						Mathf.Max(o.a - delta, normalColor.a));
			}
		}
		else {
			c = normalColor;
		}
		guiTexture.color = c;
		guiTexture.color = c;
	}

	void OnMouseEnterMoldButton() {
		mouse_in = true;
		Debug.Log(gameObject.name + ": Mouse enter");
	}

	void OnMouseLeaveMoldButton() {
		mouse_in = false;
		Debug.Log(gameObject.name + ": Mouse leave");
	}

	void OnMouseOverMoldButton() {
		// Debug.Log(gameObject.name + ": Mouse over");
	}

	void OnMouseClickMoldButton() {
		Debug.Log(gameObject.name + ": Mouse click");

		if (exitOnClick) {
			Application.Quit();
		}

		if (changeBgTarget && changeBgTo) {
			changeBgTarget.texture = changeBgTo;
		}

		if (switchToOnClickGroup) {
			foreach (Transform t in switchToOnClickGroup)
				if (t != switchToOnClick)
					t.gameObject.SetActive(false);
		}

		if (switchToOnClick) {
			switchToOnClick.gameObject.SetActive(true);
			Animation a = switchToOnClick.gameObject.animation;
			if (a) {
				if (switchToOnClickRewindAnim) {
					a[a.clip.name].time = 0.0f;
					a.Sample();
				}
				a[a.clip.name].enabled = switchToOnClickPlayAnim;
			}
			if (mainCamera) {
				mainCamera.dragging = switchToOnClickDraggable;
				if (!switchToOnClickDraggable)
					mainCamera.ResetPosition();
			}
			if (null != switchToOnClickEnableBehaviours)
				foreach (Behaviour b in switchToOnClickEnableBehaviours)
					b.enabled = true;
			if (null != switchToOnClickDisableBehaviours)
				foreach (Behaviour b in switchToOnClickDisableBehaviours)
					b.enabled = false;
		}

		/* GameObject g = test.gameObject;
		g.SetActive(!g.activeInHierarchy); */
	}

	void OnGUI() {
		if (mouse_in && !string.IsNullOrEmpty(description)) {
			GUI.skin = skin;
			Vector2 center = SGUIUtils.GetGUITexCenter(guiTexture);
			GUI.Label(new Rect(
						center.x + descriptionGeometry.x,
						Screen.height - center.y + descriptionGeometry.y,
						descriptionGeometry.width, descriptionGeometry.height),
					description);
		}
	}
}
