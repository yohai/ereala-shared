using UnityEngine;
using System.Collections;

/// <summary> 
/// metaio callback class to switch camera mode or handle torch on Android.
/// </summary>
public class metaioCallbackEn : metaioCallback {
	private bool ready = false;
	private bool torch = false;

	protected override void onSDKReady() {
		Debug.Log("Now SDK is ready!");
		ready = true;
	}

	public static AndroidJavaObject GetActivity() {
		AndroidJavaClass jc =
			new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		return jc.GetStatic<AndroidJavaObject>("currentActivity");
	}

	public static void SetTorch(bool enable) {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass clssdk =
			new AndroidJavaClass("com.metaio.sdk.jni.IMetaioSDKAndroid");
		clssdk.CallStatic((enable ? "startTorch": "stopTorch"), GetActivity());
#endif
	}

	public static void SetCameraMode(string mstr) {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass clssdk =
			new AndroidJavaClass("com.metaio.sdk.jni.IMetaioSDKAndroid");
		AndroidJavaObject camera
			= clssdk.CallStatic<AndroidJavaObject>("getCamera", GetActivity());
		AndroidJavaClass clsicc =
			new AndroidJavaClass("com.metaio.sdk.jni.IImageCaptureComponent");
		int mode = clsicc.GetStatic<int>(mstr);
		if (null != camera) {
			camera.Call("setFocusMode", mode);
		}
#endif
	}

	/*
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
	*/

	void OnGUI() {
		if (!ready) return;

#if UNITY_ANDROID
		GUILayout.BeginArea (new Rect (Screen.width - 100, Screen.height - 300, 100, 300));
		GUILayout.BeginVertical();
		if (GUILayout.Button("Unknown"))
			SetCameraMode("FOCUS_MODE_UNKNOWN");
		if (GUILayout.Button("Auto"))
			SetCameraMode("FOCUS_MODE_AUTO");
		if (GUILayout.Button("Infinity"))
			SetCameraMode("FOCUS_MODE_INFINITY");
		if (GUILayout.Button("Macro"))
			SetCameraMode("FOCUS_MODE_MACRO");

		if (GUILayout.Button("Torch")) {
			torch = !torch;
			SetTorch(torch);
		}
		GUILayout.EndVertical();
		GUILayout.EndArea ();
#endif
	}

}
