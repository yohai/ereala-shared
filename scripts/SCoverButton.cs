using UnityEngine;
using System.Collections;

// TODO: This is work-in-progress code!
/// <summary>
/// Detect users' button press on a piece of tracked paper.</summary>
public class SCoverButton : MonoBehaviour {
	public int coreCosID;
	public int[] buttonCosIDs;
	public float qualityThreshold = 0.0f;
	public MeshRenderer debugcube;

	public delegate void buttonPressed(int cosID);
	public buttonPressed OnButtonPressed;

	// Use this for initialization
	void Start() {
		if (debugcube) {
			OnButtonPressed += DbgOnButtonPressed;
		}
	}
	
	// Update is called once per frame
	void Update() {
		if (0 == buttonCosIDs.Length || !CosIsTracked(coreCosID))
			return;
		int num = buttonCosIDs.Length;
		bool[] status = new bool[num];
		for (int i = 0; i < num; ++i)
			status[i] = CosIsTracked(buttonCosIDs[i]);
		if (debugcube) {
			string s = CosIsTracked(coreCosID) + "";
			foreach (bool b in status)
				s += (b ? 1: 0);
			print(s);
		}
		for (int i = 0; i < status.Length; ++i) {
			// If a button couldn't be tracked but either one on sides could be
			// (Remember a person has two hands!), assume the button is
			// pressed.
			if (!status[i] && (status[(i + 1) % num] ||
						status[(i - 1 + num) % num]))
				OnButtonPressed(i);
		}
	}

	public void DbgOnButtonPressed(int cosID) {
		switch (cosID) {
			case 1:
				debugcube.material.SetColor("_Color",
						new Color(1.0f, 0.0f, 0.0f, 0.5f));
				break;
			case 2:
				debugcube.material.SetColor("_Color",
						new Color(0.0f, 0.0f, 1.0f, 0.5f));
				break;
		}
		// Debug.Log(cosID + " pressed.");
	}

	public bool CosIsTracked(int cosID) {
		return GetTrackingQuality(cosID) > qualityThreshold;
	}

	public static float GetTrackingQuality(int cosID) {
		float[] trackingValues = new float[7];
		return metaioSDK.getTrackingValues(cosID, trackingValues);
	}
}
