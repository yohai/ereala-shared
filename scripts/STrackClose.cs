using UnityEngine;
using System.Collections;

public class STrackClose : MonoBehaviour {
	public metaioTracker tracker_self;
	public metaioTracker[] trackers;
	public float range = 250.0f;

	// Use this for initialization
	void Start () {
	}

	protected void SetClose(bool close) {
		Color c = new Color(1.0f, 0.612f, 0.612f, 0.37f);
		if (close)
			c = new Color(0.612f, 1.0f, 1.0f, 1.0f);
		renderer.material.color = c;
	}

	// Update is called once per frame
	void Update () {
		if (!tracker_self.gameObject.activeInHierarchy || !tracker_self.enabled)
			return;

		float[] trackingvals = new float[7];
		if (metaioSDK.getTrackingValues(tracker_self.cosID, trackingvals) <= 0.0f)
			return;

		Vector3 pself = new Vector3(trackingvals[0], trackingvals[1], trackingvals[2]);
		foreach (metaioTracker t in trackers)
			if (t.gameObject.activeInHierarchy && t.enabled
					&& metaioSDK.getTrackingValues(t.cosID, trackingvals) > 0.0f) {
				Vector3 p = new Vector3(trackingvals[0], trackingvals[1], trackingvals[2]);
				print((p - pself).magnitude);
				if ((p - pself).magnitude <= range) {
					SetClose(true);
					return;
				}
			}
		SetClose(false);
	}
}
