﻿using UnityEngine;
using System.Collections;

public class SMoldSwitchBar : MonoBehaviour {
	public GUISkin skin;
	public Rect posProportional = new Rect(0.0f, 0.5f, 0.0f, 0.0f);
	public Rect posOffset = new Rect(0.0f, - 100.0f, 50.0f, 200.0f);
	public bool vertical = false;
	public GUIContent[] buttons;
	public Transform[] switchToOnClicks;
	public Transform switchToOnClickGroup = null;
	public bool switchToOnClickRewindAnim = false;
	public bool switchToOnClickPlayAnim = false;

	// Use this for initialization
	void Start() {
	}

	// Update is called once per frame
	void Update() {
	}

	void OnGUI() {
		GUI.skin = skin;
		GUILayout.BeginArea(
				SGUIUtils.MapScreenRect(posProportional, posOffset));
		string name = "";
		if (vertical) GUILayout.BeginVertical(name);
		else GUILayout.BeginHorizontal(name);
		for (int i = 0; i < buttons.Length; ++i) {
			if (GUILayout.Button(buttons[i]))
				SwitchTo(i);
		}
		if (vertical) GUILayout.EndVertical();
		else GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	void SwitchTo(int index) {
		Transform switchToOnClick = switchToOnClicks[index];

		if (switchToOnClickGroup) {
			foreach (Transform t in switchToOnClickGroup)
				if (t != switchToOnClick)
					t.gameObject.SetActive(false);
		}

		if (switchToOnClick) {
			switchToOnClick.gameObject.SetActive(true);
			Animation a = switchToOnClick.gameObject.animation;
			if (a) {
				if (switchToOnClickRewindAnim) {
					a[a.clip.name].time = 0.0f;
					a.Sample();
				}
				a[a.clip.name].enabled = switchToOnClickPlayAnim;
			}
		}
	}
}
