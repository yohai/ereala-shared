﻿using UnityEngine;
using System.Collections;

public class SGUIUtils : MonoBehaviour {
	public static float alpha_threshold = 0.1f;

	public static Vector2 GetGUITexCenter(GUITexture gtex) {
		Transform t = gtex.gameObject.transform;
		return new Vector2(
					t.position.x * Screen.width
						+ gtex.pixelInset.x + gtex.pixelInset.width / 2.0f,
					t.position.y * Screen.height
						+ gtex.pixelInset.y + gtex.pixelInset.height / 2.0f);
	}

	public static Rect GetGUITexBounds(GUITexture gtex) {
		Transform t = gtex.gameObject.transform;
		return new Rect(
					t.position.x * Screen.width
						+ gtex.pixelInset.x,
					t.position.y * Screen.height
						+ gtex.pixelInset.y,
					gtex.pixelInset.width, gtex.pixelInset.height);
	}

	public static Vector2 GetTexCoord(Rect bound, Vector3 mouse_pos) {
		return new Vector2((mouse_pos.x - bound.x) / bound.width,
				(mouse_pos.y - bound.y) / bound.height);
	}

	public static Rect RectFromCenter(Vector2 center,
			float width, float height) {
		return new Rect(center.x - width / 2.0f, center.y - height / 2.0f,
				width, height);
	}

	public static bool AlphaGUITextureMouseDetect(GUITexture gtex) {
		Rect bounds = SGUIUtils.GetGUITexBounds(gtex);
		if (!bounds.Contains(Input.mousePosition)) return false;
		Vector2 coord = SGUIUtils.GetTexCoord(bounds, Input.mousePosition);
		Texture2D tex = (Texture2D) gtex.texture;
		Color pixel = tex.GetPixel((int) (coord.x * tex.width),
				(int) (coord.y * tex.height));
		if (pixel.a < alpha_threshold) return false;
		return true;
	}

	public static Vector2 MapScreenV2(Vector2 proportion, Vector2 offset) {
		return new Vector2(proportion.x * Screen.width + offset.x,
				proportion.y * Screen.height + offset.y);
	}

	public static Rect MapScreenRect(Rect proportion, Rect offset) {
		return new Rect(proportion.x * Screen.width + offset.x,
				proportion.y * Screen.height + offset.y,
				proportion.width * Screen.width + offset.width,
				proportion.height * Screen.height + offset.height);
	}

}
