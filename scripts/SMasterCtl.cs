using UnityEngine;
using System.Collections;

public class SMasterCtl : MonoBehaviour {
	public Camera secCamera = null;
	public Vector3 maincmpos;
	public Quaternion maincmrot;
	public Vector3 seccmpos;
	public Quaternion seccmrot;

	public GUISkin uiskin = null;

	public int dragbut = 0;

	public int mvbut = 2;
	public Camera mvcm = null;
	public Vector2 mvorig;
	public float mvfactor = 0.1f;

	public int rotbut = 1;
	public Camera rotcm = null;
	public Vector2 rotorig;
	public float rotfactor = 0.1f;

	public float mwspeed = 5.0f;
	public float mwcur = 0.0f;
	public float mwfriction = 0.7f;

	public float dblclkintv = 0.2f;
	public float dblclktime = 0.0f;

	public bool lmt = false;
	public Vector3 lmtmin;
	public Vector3 lmtmax;

	private const int NTXTF = 4;
	private string[] txtfvals = new string[NTXTF];
	private bool[] txtfinvalid = new bool[NTXTF];

	public Camera GetCm() {
		Camera cm = Camera.main;
		/* if (secCamera && secCamera.rect.Contains(new Vector2(
						Input.mousePosition.x / Screen.width,
						Input.mousePosition.y / Screen.height))) */
		if (secCamera && secCamera.pixelRect.Contains(Input.mousePosition))
			cm = secCamera;
		return cm;
	}

	// Use this for initialization
	void Start() {
		for (int i = 0; i < txtfvals.Length; ++i)
			txtfvals[i] = "";
		txtfvals[0] = "0.0";
		txtfvals[1] = "0.0";
		txtfvals[2] = "0.0";
		txtfvals[3] = "0.0";

		maincmpos = Camera.main.transform.position;
		maincmrot = Camera.main.transform.rotation;
		if (null != secCamera) {
			seccmpos = secCamera.transform.position;
			seccmrot = secCamera.transform.rotation;
		}
	}
	
	// Update is called once per frame
	void Update() {
		// Mousewheel resize
		{
			Camera cm = GetCm();
			float wheelpos = Input.GetAxis("Mouse ScrollWheel");
			Vector3 cmpos = cm.transform.position;
			mwcur += wheelpos * mwspeed;
			cmpos.z -= mwcur;
			cm.transform.position = cmpos;
			mwcur *= Mathf.Pow(mwfriction, Time.deltaTime);
		}

		// Drag move
		{
			if (Input.GetMouseButtonDown(mvbut)) {
				mvorig = Input.mousePosition;
				mvcm = GetCm();
			}
			else if (Input.GetMouseButton(mvbut)) {
				Camera cm = mvcm;
				Vector3 pos = cm.transform.position;
				pos.x += Time.deltaTime * mvfactor
				   	* (Input.mousePosition.x - mvorig.x);
				pos.y += Time.deltaTime * mvfactor
					* (Input.mousePosition.y - mvorig.y);
				cm.transform.position = pos;
			}
		}

		// Drag rotate
		{
			if (Input.GetMouseButtonDown(rotbut)) {
				rotorig = Input.mousePosition;
				rotcm = GetCm();
			}
			else if (Input.GetMouseButton(rotbut)) {
				Camera cm = rotcm;
				Quaternion rot = cm.transform.rotation;
				rot *= Quaternion.Euler(
						Time.deltaTime * rotfactor
							* - (Input.mousePosition.y - rotorig.y),
						Time.deltaTime * rotfactor
							* (Input.mousePosition.x - rotorig.x),
						0.0f);
				cm.transform.rotation = rot;
			}
		}

		// Double click
		if (Input.GetMouseButtonUp(0)) {
			if (Time.time < dblclktime + dblclkintv) {
				print("Triggered!");
				Camera cm = GetCm();
				if (null != secCamera && secCamera == cm) {
					cm.transform.position = seccmpos;
					cm.transform.rotation = seccmrot;
				}
				else {
					cm.transform.position = maincmpos;
					cm.transform.rotation = maincmrot;
				}
				dblclktime = 0.0f;
			}
			else {
				dblclktime = Time.time;
			}
		}

		// Drag item
		/*
		if (Input.GetMouseButtonDown(dragbut)) {
			Camera cm = GetCm();
			RaycastHit rhit;
			if (Physics.Raycast(cm.ScreenPointToRay(Input.mousePosition),
						out rhit, 100)) {
				if (null != rhit.rigidbody) {
					print("Catch it!");
				}
			}
		} */
		if (lmt) {
			Camera cm = GetCm();
			Vector3 p = cm.transform.position;
			p = new Vector3(
					Mathf.Clamp(p.x, lmtmin.x, lmtmax.x),
					Mathf.Clamp(p.y, lmtmin.y, lmtmax.y),
					Mathf.Clamp(p.z, lmtmin.z, lmtmax.z));
			cm.transform.position = p;
		}
	}

	static Rect RectIvY(Rect r) {
		return new Rect(r.x, Screen.height - r.y - r.height, r.width, r.height);
	}

	static Rect RectExp(Rect r, float expand) {
		return new Rect(r.x - expand, r.y - expand,
				r.width + expand * 2, r.height + expand * 2);
	}

	static Rect RectCropScr(Rect r) {
		r.xMin = Mathf.Max(r.xMin, 0.0f);
		r.xMax = Mathf.Min(r.xMax, Screen.width);
		r.yMin = Mathf.Max(r.yMin, 0.0f);
		r.yMax = Mathf.Min(r.yMax, Screen.height);
		return r;
	}

	bool UILabelFloat(int i, string lbltext, out float oval) {
		Color bc = GUI.backgroundColor;
		GUILayout.BeginHorizontal();
		GUILayout.Label(lbltext);
		if (txtfinvalid[i])
			GUI.backgroundColor = Color.red;
		txtfvals[i] = GUILayout.TextField(txtfvals[i]);
		bool success = float.TryParse(txtfvals[i], out oval);
		txtfinvalid[i] = !success;
		GUILayout.EndHorizontal();
		GUI.backgroundColor = bc;
		return success;
	}

	void OnGUI() {
		/*
		GUI.skin = uiskin;

		// Second camera rectangle
		if (secCamera) {
			Rect r = RectCropScr(RectExp(RectIvY(secCamera.pixelRect), 20));
			GUI.Box(r, "Second camera");
		}

		// Control panel
		{
			GUIStyle stytitle = GUI.skin.GetStyle("Label");
			stytitle.alignment = TextAnchor.UpperCenter;
			stytitle.fontStyle = FontStyle.Bold;

			float fval = 0.0f, fval2 = 0.0f;
			GUILayout.BeginArea(new Rect(Screen.width - 220, 20, 200, 600));
			GUILayout.BeginVertical("box");
			GUILayout.Label("The ugly control panel", stytitle);
			GUI.enabled = true;
			GUI.enabled = UILabelFloat(0, "Speed:", out fval);
			if (GUILayout.Button("Set outer transform speed") && GUI.enabled)
				foreach (SAnimWrap a in anmis)
					a.o_speed = fval;
			GUI.enabled = true;
			GUI.enabled = UILabelFloat(1, "Speed:", out fval);
			if (GUILayout.Button("Set element transform speed") && GUI.enabled)
				foreach (SAnimWrap a in anmis)
					a.e_speed = fval;
			GUI.enabled = true;
			GUI.enabled = UILabelFloat(2, "Speed:", out fval);
			GUI.enabled = UILabelFloat(3, "Range:", out fval2) && GUI.enabled;
			if (GUILayout.Button("Set texture transform factors") && GUI.enabled)
				foreach (SAnimWrap a in anmis) {
					a.t_speed = fval;
					a.t_rg = fval2;
				}
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
		*/
		if (GUI.Button(new Rect(Screen.width - 50, Screen.height - 50, 50, 50), "Exit"))
			Application.Quit();
	}
}
