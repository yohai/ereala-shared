﻿using UnityEngine;
using System.Collections;

public class SMoldMainButton : MonoBehaviour {
	/// <summary>
	/// Radian representation of 360'.
	/// </summary>
	public const float RAD = Mathf.PI * 2.0f;

	/*
	public bool percent_positioning = false;
	public Vector2 percent_positioning_pos;
	public Vector2 percent_positioning_ofst;
	*/

	/// <summary>
	/// Transform menu items position according to menu button position.
	/// </summary>
	public bool forceCameraTransformPosition = true;

	/// <summary>
	/// Main camera controller in the scene, used for mouse grabbing.
	/// </summary>
	public SMoldMainCamera mainCamera;

	/// <summary>
	/// Items in the menu.</summary>
	public GUITexture[] buttons;

	/// <summary>
	/// Radius of the menu circle.</summary>
	public float radius = 100.0f;

	/// <summary>
	/// Additional radius to consider the mouse is in sensitive area.
	/// </summary>
	public float mousePositionTolerance = 20.0f;

	/// <summary>
	/// Time to animation menu opening/closing.</summary>
	public float animationTime = 2.0f;

	public bool draggable = false;

	public float angleOffset = 0.0f;
	public float angleRange = 360.0f;

	protected float speed_factor = 0.0f;
	protected float progress = 0.0f;
	protected GUITexture last_hover;
	public enum ClickStatus { No, Moved, Real };
	protected ClickStatus click_status = ClickStatus.No;
	protected Vector2 lastMousePosition;

	/// <summary>
	/// Whether the script is currently grabbing the mouse.</summary>
	protected bool mouseGrabbed = false;

	// Use this for initialization
	void Start() {
	}

	float GetMouseAngle(Vector2 mousep) {
		Vector2 center = SGUIUtils.GetGUITexCenter(guiTexture);
		return Mathf.Atan2(mousep.y - center.y, mousep.x - center.x);
	}

	// Update is called once per frame
	void Update() {
		/* if (null == Event.current || EventType.Repaint != Event.current.type)
			return; */

		/* // Percentage-based positioning of center element
		if (percent_positioning) {
			// transform.position = 0
		} */

		// Detect if mouse is going out
		// TODO: Use distance detection instead?
		Rect texreg = SGUIUtils.GetGUITexBounds(guiTexture);
		Rect sensitive_reg = SGUIUtils.RectFromCenter(texreg.center,
				texreg.width + progress * 2.0f + mousePositionTolerance * 2.0f,
				texreg.height + progress * 2.0f + mousePositionTolerance * 2.0f);
		bool mouse_in = sensitive_reg.Contains(Input.mousePosition);
		speed_factor = (mouse_in ? 1.0f: -1.0f);

		// Calculate new progress
		progress = Mathf.Clamp(progress + radius / animationTime * speed_factor * Time.deltaTime, 0.0f, radius);

		if (null == buttons) return;

		bool mouse_down = Input.GetMouseButton(0);

		// Click message
		if (!mouse_down) {
			if (ClickStatus.Real == click_status && last_hover)
				last_hover.gameObject.SendMessage("OnMouseClickMoldButton");
			click_status = ClickStatus.No;
			ReleaseMouse();
		}

		// Avoid detecting hover on other buttons when the mouse is over
		// main button
		GUITexture cur_hover = null;
		if (SGUIUtils.AlphaGUITextureMouseDetect(guiTexture))
			cur_hover = guiTexture;

		{
			int count = 0;
			for (int j = 0; j < buttons.Length; ++j)
				if (buttons[j].gameObject.activeInHierarchy)
					++count;

			int i = 0;
			foreach (GUITexture b in buttons) {
				Transform t = b.gameObject.transform;

				if (!b.gameObject.activeInHierarchy) continue;

				++i;

				// Re-adjust button position and size
				// Currently use pixel inset
				if (forceCameraTransformPosition)
					t.position = new Vector3(
							transform.position.x,
							transform.position.y,
							transform.position.z - 10.0f);
				float angle =
					((float) i / count * angleRange + angleOffset)
					/ 360.0f * RAD;
				float width = b.texture.width * progress / radius;
				float height = b.texture.height * progress / radius;
				float x = guiTexture.pixelInset.x
					+ guiTexture.pixelInset.width / 2.0f
					+ progress * Mathf.Cos(angle) - width * 0.5f;
				float y = guiTexture.pixelInset.y
					+ guiTexture.pixelInset.height / 2.0f
					+ progress * Mathf.Sin(angle) - height * 0.5f;
				b.pixelInset = new Rect(x, y, width, height);

				// Check for mouse hover
				if (!cur_hover && SGUIUtils.AlphaGUITextureMouseDetect(b))
					cur_hover = b;
			}
		}

		// Mouse hover messages
		if (cur_hover != last_hover) {
			if (last_hover)
				last_hover.gameObject.SendMessage("OnMouseLeaveMoldButton");
			if (cur_hover)
				cur_hover.gameObject.SendMessage("OnMouseEnterMoldButton");
			if (ClickStatus.Real == click_status)
				click_status = ClickStatus.Moved;
		}

		if (cur_hover)
			cur_hover.gameObject.SendMessage("OnMouseOverMoldButton");
		last_hover = cur_hover;

		if (mouse_down) {
			// Initial mouse down
			if (ClickStatus.No == click_status
					&& mouse_in && Input.GetMouseButtonDown(0)) {
				if (GrabMouse())
					click_status = ClickStatus.Real;
			}
			// Dragging
			else if (draggable && ClickStatus.No != click_status) {
				angleOffset += (GetMouseAngle(Input.mousePosition)
					- GetMouseAngle(lastMousePosition)) / RAD * 360.0f;
			}

			lastMousePosition = Input.mousePosition;
		}
	}

	bool GrabMouse() {
		if (mainCamera) {
			if (mainCamera.mouseOccupied > 0) return false;
			++mainCamera.mouseOccupied;
			mouseGrabbed = true;
		}
		return true;
	}

	void ReleaseMouse() {
		if (mouseGrabbed && mainCamera) {
			--mainCamera.mouseOccupied;
			mouseGrabbed = false;
			System.Diagnostics.Debug.Assert(mainCamera.mouseOccupied >= 0);
		}
	}

	void OnMouseEnterMoldButton() { }
	void OnMouseLeaveMoldButton() { }
	void OnMouseOverMoldButton() { }
	void OnMouseClickMoldButton() { }
}
