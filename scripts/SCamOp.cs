using UnityEngine;
using System.Collections;

public class SCamOp : MonoBehaviour {
	protected float scale = 1.0f;
	protected Vector2 scalept;

	// Use this for initialization
	void Start () {
	}

	void Update() {
		UpdScale();
	}

	void OnGUI() {
		if (scale != 1.0f)
			GUI.Box(new Rect(0.0f, 0.0f, 100.0f, 100.0f), "Boxy!");
	}

	void UpdScale() {
		scale = 1.0f;
		scalept = new Vector2(Screen.width / 2.0f, Screen.height / 2.0f);
		if (Input.GetMouseButton(0)) {
			scale *= 3.0f;
			scalept = Input.mousePosition;
		}
		foreach (Touch t in Input.touches)
			if (t.phase != TouchPhase.Ended && t.phase != TouchPhase.Canceled) {
				scalept = t.position;
				scale *= 3.0f;
			}
	}

	// Update is called once per frame
	void LateUpdate() {
		// Matrix4x4 m = PerspectiveOffCenter(-1, 1, -1, 1, camera.nearClipPlane, camera.farClipPlane, scale);
		Matrix4x4 m = Matrix4x4.Perspective(camera.fieldOfView, Screen.width / Screen.height, camera.nearClipPlane, camera.farClipPlane);
		m[0, 0] *= scale;
		m[1, 1] *= scale;
		Vector2 nmscalept = new Vector2(
				scalept.x / Screen.width - 0.5f,
				scalept.y / Screen.height - 0.5f);
		/* DebugConsole.Log(nmscalept);
		DebugConsole.IsOpen = true; */
		camera.projectionMatrix = Translate(m, - nmscalept.x, - nmscalept.y, 0.0f);
	}

	static Matrix4x4 Translate(Matrix4x4 orig, float x, float y, float z) {
		Matrix4x4 m = Matrix4x4.identity;
		m[0, 3] = x;
		m[1, 3] = y;
		m[2, 3] = z;
		return m * orig;
	}

	static Matrix4x4 PerspectiveOffCenter(float left, float right, float bottom, float top, float near, float far, float scale) {
		float x = 2.0F * near / (right - left) * scale;
		float y = 2.0F * near / (top - bottom) * scale;
		float a = (right + left) / (right - left);
		float b = (top + bottom) / (top - bottom);
		float c = -(far + near) / (far - near);
		float d = -(2.0F * far * near) / (far - near);
		float e = -1.0F;
		Matrix4x4 m = new Matrix4x4();
		m[0, 0] = x; m[0, 1] = 0; m[0, 2] = a; m[0, 3] = 0;
		m[1, 0] = 0; m[1, 1] = y; m[1, 2] = b; m[1, 3] = 0;
		m[2, 0] = 0; m[2, 1] = 0; m[2, 2] = c; m[2, 3] = d;
		m[3, 0] = 0; m[3, 1] = 0; m[3, 2] = e; m[3, 3] = 0;
		return m;
	}
}
