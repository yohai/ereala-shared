using UnityEngine;
using System.Collections;

public class metaioTrackerLLADbg : metaioTrackerLLA {
	public bool debugPose = false;
	private float[] trackingValues = new float[7];
	private float[] translation = new float[3];
	private string pstr = "";
	private string[] sll = new string[2];
	private bool[] sllbad = new bool[2];

	void Start() {
		// base.Start();
		sll[0] = "0.0";
		sll[1] = "0.0";
	}

	// Update is called once per frame
	void Update () 
	{
		pstr = "None";
		float quality = metaioSDK.getTrackingValues(1, trackingValues);
		
//		Debug.Log("Tracking quality: "+quality);
		
		
		// If quality is greater than 0, the target is detected in the current frame
		if (quality > 0)
		{
		
			// Apply rotation
			Quaternion q;
			q.x = trackingValues[3];
			q.y = trackingValues[4];
			q.z = trackingValues[5];
			q.w = trackingValues[6];
			transform.rotation = q;
			
//			Debug.Log("Rotation: "+q.ToString());
			
			// Apply cartesian translation
			Vector3 p;
			p.x = trackingValues[0];
			p.y = trackingValues[1];
			p.z = trackingValues[2];
			transform.position = p;
			
//			Debug.Log("Cartesian translation: "+transform.position.ToString());
			
			// convert LLA to cartesian translation
			metaioSDK.convertLLAToTranslation(latitude, longitude, translation);
			pstr = "(" + translation[0] + ", " + translation[1] + ", " + translation[2] + ")";
//			Debug.Log("LLA translation: "+translation[0]+", "+translation[1]+", "+translation[2]);
			
			// Augment LLA cartesian translation
			Vector3 tLLA;
			tLLA.x = translation[0];
			tLLA.y = translation[1];
			tLLA.z = translation[2];
			transform.Translate(tLLA);
			
//			Debug.Log("Final translation: "+transform.position.ToString());
			
		}
	}
	
	void OnGUI() {
		if (debugPose) {
			GUI.TextField(new Rect(10, 10, Screen.width - 20, 20), pstr);
			Color bc = GUI.backgroundColor;
			{
				if (sllbad[0])
					GUI.backgroundColor = Color.red;
				sll[0] = GUI.TextField(new Rect(10, 30, (Screen.width - 20) / 2, 20), sll[0]);
				GUI.backgroundColor = bc;
			}
			{
				if (sllbad[1])
					GUI.backgroundColor = Color.red;
				sll[1] = GUI.TextField(new Rect(10 + (Screen.width - 20) / 2, 30, (Screen.width - 20) / 2, 20), sll[1]);
				GUI.backgroundColor = bc;
			}
			float fval = 0.0f;
			{
				if (float.TryParse(sll[0], out fval)) {
					latitude = fval;
					sllbad[0] = false;
				}
				else
					sllbad[0] = true;
			}
			{
				if (float.TryParse(sll[1], out fval)) {
					longitude = fval;
					sllbad[1] = false;
				}
				else
					sllbad[1] = true;
			}
		}
	}
}
