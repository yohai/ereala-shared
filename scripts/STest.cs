﻿using UnityEngine;
using System.Collections;

public class STest : MonoBehaviour {
	public Texture aTexture;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
	}

	void OnGUI() {
		if (Event.current.type.Equals(EventType.Repaint)) {
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.modelview = Matrix4x4.TRS(
					new Vector3(100.0f, 0.0f, 0.0f),
					// Vector3.zero,
					Quaternion.Euler(0.0f, 0.0f, 30.0f),
					Vector3.one);
            Graphics.DrawTexture(new Rect(50, 50, 150, 150), aTexture);
			GL.PopMatrix();
		}
	}
}
