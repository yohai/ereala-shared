using UnityEngine;
using System.Collections;

public class SEnableOnTracked : MonoBehaviour {
	public metaioTracker[] trackers;
	public Transform[] eles;
	public bool disable_on_lost = false;
	public bool syncpos = false;
	public Vector3 syncpos_offset;
	private bool tracked = false;
	private bool tracked_once = false;
	private float[] trackingvals = new float[7];

	// Use this for initialization
	void Start() {
	}

	protected void ModEles(bool enabled) {
		if (eles.Length > 0)
			foreach (Transform t in eles)
				t.gameObject.SetActive(enabled);
		else
			foreach (Transform t in transform)
				t.gameObject.SetActive(enabled);
	}

	// Update is called once per frame
	void Update() {
		if (Input.GetMouseButtonDown(0)) {
			foreach (metaioTracker t in trackers)
				if (t.gameObject.activeInHierarchy && t.enabled
						&& metaioSDK.getTrackingValues(t.cosID, trackingvals) > 0.0f) {
					Vector3 p = new Vector3(trackingvals[0], trackingvals[1], trackingvals[2]);
					transform.position = p + syncpos_offset;
					break;
				}
		}

		if (!disable_on_lost && tracked) return;

		tracked = false;
		foreach (metaioTracker t in trackers)
			if (t.gameObject.activeInHierarchy && t.enabled
					&& metaioSDK.getTrackingValues(t.cosID, trackingvals) > 0.0f) {
				tracked = true;
				break;
			}

		ModEles(tracked);

		if (tracked && !tracked_once) {
			tracked_once = true;
			if (syncpos) {
				Vector3 p = new Vector3(trackingvals[0], trackingvals[1], trackingvals[2]);
				transform.position = p + syncpos_offset;
			}
		}
	}
}
