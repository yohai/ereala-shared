﻿using UnityEngine;
using System.Collections;

public class SMoldMainCamera : MonoBehaviour {
	public int mouseOccupied = 0;
	public Vector3 originalPosition;
	public Quaternion originalRotation;
	public bool dragging = true;
	public enum Axis { X, Y, Z };
	public Transform draggingCenter;
	public Vector3 draggingCenterOffset;
	public Axis draggingXAxisTo = Axis.Y;
	public Axis draggingYAxisTo = Axis.X;
	public float draggingXAxisMin = 0.0f;
	public float draggingXAxisMax = 360.0f;
	public float draggingYAxisMin = 0.0f;
	public float draggingYAxisMax = 70.0f;
	public Rect[] draggingIgnoreRegionsProportional;
	public Rect[] draggingIgnoreRegionsOffset;
	public float wheelMoveSpeed = 1.0f;
	public float radiusMax = 15.0f;
	public float radiusMin = 3.0f;

	protected bool mouseGrabbed = false;
	protected Vector2 lastMousePosition;
	protected bool mouseIsDragging = false;

	// Use this for initialization
	void Start() {
		originalPosition = transform.position;
		originalRotation = transform.rotation;
	}

	public void ResetPosition() {
		transform.position = originalPosition;
		transform.rotation = originalRotation;
	}

	float ClampAngle(float angle, float min, float max) {
		if (min > max) {
			float t = min;
			min = max;
			max = t;
		}
		if (min <= 0.0f && max >= 360.0f)
			return angle - Mathf.Floor(angle / 360.0f) * 360.0f;
		return Mathf.Clamp(angle, min, max);
	}

	bool IsMouseInDraggingIgnoreRegion() {
		Vector2 mouse_pos = Input.mousePosition;
		mouse_pos = new Vector2(mouse_pos.x, Screen.height - mouse_pos.y);
		if (null != draggingIgnoreRegionsProportional) {
			for (int i = 0; i < draggingIgnoreRegionsProportional.Length; ++i) {
				Rect r = SGUIUtils.MapScreenRect(
						draggingIgnoreRegionsProportional[i],
						draggingIgnoreRegionsOffset[i]);
				if (r.Contains(mouse_pos))
					return true;
			}
		}
		return false;
	}

	void UpdatePosition() {
		float wheel_val = 0.0f;
		if (dragging && Mathf.Abs(wheelMoveSpeed) > 0.01f)
			wheel_val = Input.GetAxis("Mouse ScrollWheel");
		if (!(mouseIsDragging || Mathf.Abs(wheel_val) > 0.01f))
			return;

		Vector2 cur = lastMousePosition;
		if (mouseIsDragging)
			cur = Input.mousePosition;
		Vector3 center = draggingCenterOffset;
		if (draggingCenter)
			center += draggingCenter.position;
		float radius = Mathf.Clamp(
				(transform.position - center).magnitude + wheelMoveSpeed * wheel_val,
			 	radiusMin, radiusMax);
		Vector3 cur_euler = (Quaternion.LookRotation(
					center - transform.position).eulerAngles);
		float diff_x = (cur.x - lastMousePosition.x) / Screen.width
			* (draggingXAxisMax - draggingXAxisMin);
		float diff_y = (cur.y - lastMousePosition.y) / Screen.height
			* (draggingYAxisMax - draggingYAxisMin);
		cur_euler.y = ClampAngle(cur_euler.y + diff_x,
				draggingXAxisMin, draggingXAxisMax);
		cur_euler.x = ClampAngle(cur_euler.x + diff_y,
				draggingYAxisMin, draggingYAxisMax);
		Quaternion rotation = Quaternion.Euler(cur_euler);
		Vector3 offset = rotation * (new Vector3(0.0f, 0.0f, - radius));
		transform.position = center + offset;
		transform.rotation = rotation;
		lastMousePosition = cur;
	}

	// Update is called once per frame
	void LateUpdate() {
		System.Diagnostics.Debug.Assert(mouseOccupied >= 0);

		// Detect dragging
		// Stop dragging
		if (mouseIsDragging && !(Input.GetMouseButton(0) && dragging)) {
			mouseIsDragging = false;
			ReleaseMouse();
		}
		// Start dragging
		else if (!mouseIsDragging && dragging
				&& Input.GetMouseButtonDown(0)
				&& !IsMouseInDraggingIgnoreRegion()
				&& GrabMouse()) {
			mouseIsDragging = true;
			// Overwrite mouse position here to avoid movement on first frame
			lastMousePosition = Input.mousePosition;
		}
		UpdatePosition();
	}

	bool GrabMouse() {
		if (mouseOccupied > 0) return false;
		++mouseOccupied;
		mouseGrabbed = true;
		return true;
	}

	void ReleaseMouse() {
		if (mouseGrabbed) {
			--mouseOccupied;
			mouseGrabbed = false;
			System.Diagnostics.Debug.Assert(mouseOccupied >= 0);
		}
	}

}
