using UnityEngine;
using System.Collections;

/// <summary>
/// Play animation after tracking target is found.</summary>
public class STrackAnim: MonoBehaviour {
	public float[] chtimes;
	public Transform[] exgobj;
	public float[] exgobjtm;

	// public bool recursive = false;
	private float basetm = 0.0f;
	public float rotatetime = 0.0f;
	private int lastpass = 0;
	public class Rule {
		public float time;
		public Transform t;
		public MeshRenderer mr;
		public bool triggered;
	};
	public Rule[] rules;

	private const float TM_NEVER = -9999.0f;
	private float last_lost = TM_NEVER;
	private bool once_tracked = false;
	public metaioTracker tracker;
	public float lost_tolerance = 3.0f;
	public Animation anim;

	// Use this for initialization
	void Start() {
		// Search upward for tracker
		if (null == tracker) {
			Transform t = transform;
			while (null == t.gameObject.GetComponent<metaioTracker>()) {
				if (null == (t = t.parent))
					throw new System.ApplicationException("Traveled all the way to root and still no tracker!");
			}
			tracker = t.gameObject.GetComponent<metaioTracker>();
		}

		// Initialize rules
		rules = new Rule[chtimes.Length + exgobj.Length];
		for (int i = 0; i < rules.Length; ++i)
			rules[i] = new Rule();
		int bs = 0;
		for (int i = bs; i < chtimes.Length; ++i) {
			rules[i].time = chtimes[i];
			rules[i].mr = GetComponent<MeshRenderer>();
		}
		bs += chtimes.Length;
		for (int i = bs; i < exgobj.Length + bs; ++i) {
			rules[i].time = exgobjtm[i];
			rules[i].t = exgobj[i];
		}
	}

	void RestoreAll() {
		foreach (Rule r in rules) {
			if (r.triggered)
				Invert(r);
			r.triggered = false;
		}
	}

	void Invert(Rule r) {
		if (r.t) {
			GameObject o = r.t.gameObject;
			o.SetActive(!o.activeSelf);
		}
		else if (r.mr) {
			r.mr.enabled = !r.mr.enabled;
		}
		r.triggered = true;
	}

	public void Restart() {
		RestoreAll();
		if (null != anim) {
			anim.Rewind();
			anim.Play();
		}
		basetm = Time.time;
	}

	void Update () {
		bool tracked = false;
		bool tracked_last = (once_tracked && last_lost < 0.0f);
		{
			float[] trackingValues = new float[7];
			tracked = (metaioSDK.getTrackingValues(tracker.cosID,
						trackingValues) > 0.0f);
		}

		once_tracked = tracked || once_tracked;

		if (tracked_last && !tracked) {
			last_lost = Time.time;
			if (null != anim) {
				foreach (AnimationState a in anim)
					a.speed = 0.0f;
			}
		}

		if (!tracked_last && tracked) {
			if (null != anim) {
				foreach (AnimationState a in anim) {
					a.speed = 1.0f;
				}
			}

			// Restart the whole thing
			if (last_lost + lost_tolerance < Time.time) {
				Restart();
			}
			else {
				print("Resume! " + Time.time);
				basetm += Time.time - last_lost;
			}
			last_lost = TM_NEVER;
		}

		if (tracked) {
			float tm = Time.time - basetm;
			if (rotatetime > 0.0f) {
				int pass = Mathf.FloorToInt(tm / rotatetime);
				if (pass != lastpass) {
					RestoreAll();
					lastpass = pass;
					foreach (Rule r in rules)
						r.triggered = false;
				}
				tm -= pass * rotatetime;
			}
			foreach (Rule r in rules) {
				if (!r.triggered && tm >= r.time) {
					Invert(r);
				}
			}
		}
	}
}
