Hackintosh notes
================
Richard Grenville
v0_pre20130904 Sep 4, 2013

* OS X requires SATA to be in AHCI mode. Change it in BIOS before you start. You need to link:http://www.sevenforums.com/tutorials/61869-ahci-enable-windows-7-vista.html[adjust some settings in Windows 7] to boot with AHCI, if you were using IDE during Windows installation.

* I used Clover to boot the 10.8.4 installation USB and the installed system. Both legacy and UEFI boot works. One could use the official ISO, install Clover onto a flash drive (but you need a OS X system around, and copy `/EFI/CLOVER/CLOVERX64.EFI` to `/EFI/Boot/bootx64.efi` for EFI boot), or use the copy inside the installation USB on the shared drive.

[NOTE]
Clover probably doesn't boot correctly when the device is connected through USB 3.0 port. Seemingly it doesn't reliably detect the disk it booted from, so please try to avoid the situation when you have two disks with Clover installed plugged on the same computer. (Setting a wrong partition type on the one you wish to disable may help.)

* Our motherboard (Gigabyte B75-D3V) is DSDT-free, so you don't have to care about it. GTX 650 needs some special treatment: Disable GraphicInjector or InjectNVidia in Clover graphic settings, or you won't be able to boot into OS X. If you have VT-x (Intel Virtualization Technology) enabled in BIOS, add `dart=0` to boot parameters.

* Apple's partition utility doesn't work on every disk, perhaps due to some partition scheme incompatibilities. If necessary, partition the disk in another operating system (`diskutil`, `fdisk`, `gpt`, etc. in OS X may be helpful as well), and format it to HFS+ with link:https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man8/newfs_hfs.8.html[`newfs_hfs`]. (Pay attention to journaling and case-sensitivity options. You don't have access to man pages on the installation USB, so read them online.) link:http://hints.macworld.com/article.php?story=20030613121738812[This page] may also provide some hints.

* Then you just install OS X with the installation USB. Follow the installer, that's it.

* Boot into your system for the first time with Clover on USB. Grab link:http://www.tonymacx86.com/downloads.php?do=file&id=186[MultiBeast for Mountain Lion] (registration required) and install FakeSMC, NullCPUPowerManagement, ALC887, Atheros ethernet chip support (ALXEthernet?) (all DSDT-free), and probably PS/2 support. The generic XHCI (USB 3.0) driver may cause kernel panic and USB 3.0 probably works without it. ALC887 requires `dart=0` in kernel boot parameters, set later in Clover. Do not install any bootloaders from Multibeast.

* Install Clover on hard drive. Just follow the installer. Ensure you select `OsxAptioFixDrv` for EFI boot. Make sure you have at least one theme installation (otherwise it looks like shit).

* Drop kernel cache (`/System/Library/Caches/com.apple.kext.caches/Startup/kernelcache`). Reboot.

* Adjust `config.plist` in the Clover installation directory, to avoid having to change clover settings on every boot, with a text editor or XCode. Here's mine:
+
[source,xml]
--------------------------------------
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>KernelAndKextPatches</key>
	<dict>
		<key>Debug</key>
		<false/>
		<key>KernelCpu</key>
		<false/>
		<key>AsusAICPUPM</key>
		<true/>
		<key>AppleRTC</key>
		<true/>
		<key>KextsToPatch</key>
		<array>
			<dict>
				<key>Name</key>
				<string>VoodooHDA</string>
				<key>Find</key>
				<data>SGVhZHBob25lcwA=</data>
				<key>Replace</key>
				<data>VGVsZXBob25lcwA=</data>
			</dict>
			<dict>
				<key>Name</key>
				<string>AppleAHCIPort</string>
				<key>Comment</key>
				<string>External icons patch</string>
				<key>Find</key>
				<data>RXh0ZXJuYWw=</data>
				<key>Replace</key>
				<data>SW50ZXJuYWw=</data>
			</dict>
		</array>
	</dict>
	<key>ACPI</key>
	<dict>
		<key>DsdtName</key>
		<string>DSDT.aml</string>
		<key>DropOemSSDT</key>
		<false/>
		<key>DropAPIC</key>
		<false/>
		<key>DropMCFG</key>
		<false/>
		<key>DropHPET</key>
		<false/>
		<key>DropECDT</key>
		<false/>
		<key>DropDMAR</key>
		<true/>
		<key>DropBGRT</key>
		<true/>
		<key>GenerateIvyStates</key>
		<false/>
		<key>GenerateCStates</key>
		<false/>
		<key>GeneratePStates</key>
		<false/>
		<key>DoubleFirstState</key>
		<false/>
		<key>PLimitDict</key>
		<integer>0</integer>
		<key>UnderVoltStep</key>
		<integer>0</integer>
	</dict>
	<key>DisableDrivers</key>
	<array>
		<string>Nothing</string>
	</array>
	<key>RtVariables</key>
	<dict>
		<key>MountEFI</key>
		<true/>
		<key>LogLineCount</key>
		<integer>3000</integer>
		<key>LogEveryBoot</key>
		<string>Yes</string>
	</dict>
	<key>GUI</key>
	<dict>
		<key>TextOnly</key>
		<false/>
		<key>Theme</key>
		<string>black_green</string>
		<key>Timeout</key>
		<integer>5</integer>
		<key>DefaultBootVolume</key>
		<string>SnowHD</string>
		<key>DebugLog</key>
		<false/>
		<key>Mouse</key>
		<dict>
			<key>Enabled</key>
			<false/>
			<key>Speed</key>
			<integer>0</integer>
		</dict>
		<key>Volumes</key>
		<dict>
			<key>Legacy</key>
			<string>First</string>
			<key>Hide</key>
			<array>
				<string>VOLUME_NAME</string>
				<string>VOLUME_UUID</string>
				<string>TODO_HIDE_VOLUME_BY_TYPE/GROUP</string>
			</array>
		</dict>
		<key>HideEntries</key>
		<dict>
			<key>OSXInstall</key>
			<false/>
			<key>Recovery</key>
			<false/>
			<key>Duplicate</key>
			<false/>
			<key>WindowsEFI</key>
			<false/>
			<key>Grub</key>
			<false/>
			<key>Gentoo</key>
			<false/>
			<key>Ubuntu</key>
			<false/>
			<key>OpticalUEFI</key>
			<false/>
			<key>InternalUEFI</key>
			<true/>
			<key>ExternalUEFI</key>
			<false/>
		</dict>
	</dict>
	<key>PCI</key>
	<dict>
		<key>USBInjection</key>
		<true/>
		<key>USBFixOwnership</key>
		<true/>
		<key>InjectClockID</key>
		<true/>
	</dict>
	<key>SystemParameters</key>
	<dict>
		<key>boot-args</key>
		<string>-v npci=0x2000 dart=0</string>
		<key>prev-lang:kbd</key>
		<string>en:0</string>
		<key>InjectSystemID</key>
		<true/>
		<key>LegacyBoot</key>
		<string>PBR</string>
	</dict>
	<key>Graphics</key>
	<dict>
		<key>InjectNVidia</key>
		<false/>
		<key>PatchVBios</key>
		<false/>
	</dict>
</dict>
</plist>
--------------------------------------

Resources
---------

* link:http://olarila.com/forum/index.php[olarila.com], where the installation USB comes from. Feel free to ask questions there.
* link:https://github.com/tkrotoff/Gigabyte-GA-Z77-DS3H-rev1.1-Hackintosh[tkrotoff/Gigabyte-GA-Z77-DS3H-rev1.1-Hackintosh on GitHub]. Although not the same motherboard, it's moderately similar.
* link:http://wiki.osx86project.org/wiki/index.php/Main_Page[OSx86], with hardware compatibility lists.
* link:http://www.tonymacx86.com/home.php[tonymacx86], big Mac site with lots of info.
* link:http://www.insanelymac.com/[InsanelyMac], another big one.

Extra
-----

* VT2020 audio: The HDA codec I have on my home computer (Gigabyte GA-Z77X-UD3H). link:http://www.insanelymac.com/forum/topic/279133-voodoohda-for-gigabyte-ga-z77x-ud3h-via-vt2020vt2021/[Patched VoodooHDA] (available from Multibeast as well) and link:http://www.insanelymac.com/forum/topic/280004-applehda-for-gigabyte-ga-z77x-ud3h-via-vt2020vt2021/[Patched AppleHDA] both works, but the former is causing a lot of kernel panics here.
* Clipboard access does not work inside tmux: https://github.com/ChrisJohnsen/tmux-MacOSX-pasteboard
