#!/usr/bin/env python3

import sys, struct, binascii, argparse, copy, shlex, math, io, zipfile

# Constants

F3B_HEADER = bytes.fromhex('22 48 9A 00  0A 00 00 00  01 00 00 00')
F3B_SEG_LEN = 73
F3B_3DMAP_F3B_FNAME = 'map0.f3b'
F3B_3DMAP_TRACKCFG_FNAME = 'TrackingData_ML3D.xml'

# Helper functions
def tohex(data, end = '\n'):
	s = binascii.hexlify(data).decode('ascii')
	s = ' '.join(s[i:i + 2] for i in range(0, len(s), 2))
	# s = ' '.join(s[i:i + 12] for i in range(0, len(s), 12))
	return s

def my_unpack(fmt, buf):
	return struct.unpack(fmt, buf[0:struct.calcsize(fmt)])

def parse_range(s, range_max):
	def proc_idx(v):
		v = int(v)
		if v < 0:
			v = range_max + v
		return v

	if s.find(':') >= 0:
		rmin, rmax = s.split(':', 1)
		if '' == rmin: rmin = 0
		if '' == rmax: rmax = range_max
		rmin = proc_idx(rmin)
		rmax = proc_idx(rmax)
		return range(rmin, rmax)
	else:
		return (proc_idx(s), )

def parse_f3b_point(args):
	return F3BPoint(float(args[0]), float(args[1]), float(args[2]),
			bytes.fromhex(args[3]))

def log(msg, *args, **kwargs):
	print(msg.format(*args, **kwargs))

def warn(msg, *args, **kwargs):
	log(msg, *args, **kwargs)

# Classes

class BadInputException(Exception):
	pass

class ParseException(Exception):
	pass

class F3BPoint:
	identifier = '<unknown>'
	position = None
	extra_data = b''

	def __init__(self, x, y, z, extra_data):
		self.position = [x, y, z]
		self.extra_data = extra_data

	def str_prt(self):
		return '({:10g}, {:10g}, {:10g}) {}'.format(self.position[0], self.position[1], self.position[2], tohex(self.extra_data))

	def __repr__(self):
		return 'F3BPoint({:g}, {:g}, {:g}, bytes.fromhex("{}"))'.format(self.position[0], self.position[1], self.position[2], tohex(self.extra_data))

	def validate(self):
		return isinstance(self.position, list) and 3 == len(self.position) and	self.extra_data and 12 + len(self.extra_data) == F3B_SEG_LEN

	def to_data(self):
		if not self.validate():
			warn('Point invalid. Troubles ahead.')
		return struct.pack('<fff', self.position[0], self.position[1], self.position[2]) + self.extra_data

	@staticmethod
	def from_data(seg):
		upkstr = '<fff'
		upklen = struct.calcsize(upkstr)
		coordinates = struct.unpack('<fff', seg[:upklen])
		# print('{:5}: ({:10g}, {:10g}, {:10g}) {}'.format(i, coordinates[0], coordinates[1], coordinates[2], tohex(seg[12:])))
		return F3BPoint(coordinates[0], coordinates[1], coordinates[2], seg[upklen:])

class F3BData:
	header = b''
	points = None
	tracking_cfg = None

	def __init__(self):
		self.points = list()

	def dump(self):
		print('Header: {}'.format(tohex(self.header)))
		print('{} points in total.'.format(len(self.points)))
		for i in range(len(self.points)):
			pt = self.points[i]
			print('{:5}: {}'.format(i, pt.str_prt()))

	def to_data(self):
		d = self.header + struct.pack('<I', len(self.points))
		for pt in self.points:
			d += pt.to_data()
		return d

	@staticmethod
	def combine(*args):
		d = F3BData()
		d.header = args[0].header
		for f in args:
			d.points += copy.deepcopy(f.points)
		return d

	@staticmethod
	def from_data(data, identifier = None):
		if None == identifier:
			identifier = '<unknown>'

		d = F3BData()

		# Attempts to read as ZIP firstly
		try:
			with zipfile.ZipFile(io.BytesIO(data), mode='r') as z:
				log('{}: Input is a ZIP archive.', identifier)
				# Look for F3B
				fname = F3B_3DMAP_F3B_FNAME
				if fname in z.namelist():
					log('{}: {} found in ZIP archive.', identifier, fname)
					data = z.read(fname)
				# Look for tracking configuration
				fname = F3B_3DMAP_TRACKCFG_FNAME
				if fname in z.namelist():
					log('{}: {} found in ZIP archive.', identifier, fname)
					d.tracking_cfg = z.read(fname)
		except zipfile.BadZipfile:
			pass

		pos = 0

		# Parse header
		if len(data) < len(F3B_HEADER) + 4:
			raise ParseException('File too short')

		d.header = data[pos:pos + len(F3B_HEADER)]
		if d.header != F3B_HEADER:
			warn('Unrecognized header ({hdr}). Continuing anyway.', hdr = tohex(d.header))
		pos += len(F3B_HEADER)

		# Parse point count
		count = struct.unpack('<I', data[pos:pos + 4])[0]
		# log('{identifier}: I believe there are {count} points.', identifier = identifier, count = count)
		pos += 4
		expected_length = pos + count * F3B_SEG_LEN
		if len(data) != expected_length:
			warn("Expected size {} != real size {}.", expected_length, len(data))

		# Parse elements
		for i in range(0, count):
			seg = data[pos:pos + F3B_SEG_LEN]
			d.points.append(F3BPoint.from_data(seg))
			pos += F3B_SEG_LEN

		return d

	@staticmethod
	def from_file(f, identifier = None):
		return F3BData.from_data(f.read(), identifier = identifier)

	@staticmethod
	def from_path(path):
		f = open(path, 'rb')
		f3bd = F3BData.from_file(f, path)
		f.close()
		return f3bd

def read_input(args):
	dataset = list()
	if args.path:
		for path in args.path:
			if '-' != path:
				f = F3BData.from_path(path)
			else:
				f = F3BData.from_file(sys.stdin.buffer, identifier = '<stdin>')
			dataset.append(f)
	else:
		f = F3BData.from_path(args.path)
		dataset.append(f)
	return dataset

def eval_expr(f3bd, aexpr):
	cmd = aexpr[0]
	aexpr = aexpr[1:]
	ptmax = len(f3bd.points)
	if False: pass
	elif cmd in ('d', 'del', 'delete'):
		indexes = list()
		for arg in aexpr:
			indexes += parse_range(arg, ptmax)
		indexes = sorted(set(indexes))
		for i in range(len(indexes)):
			del f3bd.points[indexes[i] - i]
	elif cmd in ('i', 'ins', 'insert'):
		f3bd.points.insert(int(aexpr[0]), parse_f3b_point(aexpr[1:]))
	elif cmd in ('a', 'append'):
		f3bd.points.append(parse_f3b_point(aexpr))
	elif cmd in ('d', 'dump'):
		indexes = list()
		for arg in aexpr:
			indexes += parse_range(arg, ptmax)
		for idx in indexes:
			print('{:5}: {}'.format(idx, f3bd.points[idx].str_prt()))
	elif cmd in ('set'):
		indexes = list()
		for i in range(len(aexpr)):
			arg = aexpr[i]
			if arg.find('=') >= 0: break
			indexes += parse_range(arg, ptmax)
		for arg in aexpr[i:]:
			key, val = arg.split('=', 1)
			prefix = ''
			if not (key[-1].isalnum() or '_' == key[-1]):
				prefix = key[-1]
				key = key[:-1]
				if prefix not in ('+', '-', '*', '/'):
					warn('Unrecognized prefix "{}"', prefix)
			if 'extra_data' == key:
				for idx in indexes:
					f3bd.points[idx].extra_data = bytes.fromhex(val);
			elif key in ('x', 'y', 'z'):
				# Float items
				fval = float(val)
				if '-' == prefix:
					prefix = '+'
					fval *= -1
				if '/' == prefix:
					prefix = '*'
					fval = 1.0 / fval
				posidx = ('x', 'y', 'z').index(key)
				for idx in indexes:
					orig = f3bd.points[idx].position[posidx]
					newval = fval
					if '+' == prefix: newval += orig
					elif '*' == prefix: newval *= orig
					f3bd.points[idx].position[posidx] = newval
			else:
				warn('set: Unrecognized key {}', key)
	elif cmd in ('transform'):
		indexes = list()
		for i in range(len(aexpr)):
			arg = aexpr[i]
			if arg[0].isalpha(): break
			indexes += parse_range(arg, ptmax)
		tfmtype = aexpr[i]
		mtx = None
		args = aexpr[i + 1:]
		import numpy
		if tfmtype in ('euler'):
			vals = [ float(x) / 360.0 * 2 * math.pi for x in args[:3]]
			c = [ math.cos(x) for x in vals]
			s = [ math.sin(x) for x in vals]
			mtx = numpy.matrix([
				[ c[1] * c[2], - c[1] * s[2], s[1]],
				[ c[1] * s[2] + c[2] * s[0] * s[1], c[0] * c[2] - s[0] * s[1] * s[2], - c[1] * s[0]],
				[ s[0] * s[2] - c[0] * c[2] * s[1], c[2] * s[0] + c[0] * s[1] * s[2], c[0] * c[1]]])
		elif tfmtype in ('quaternion'):
			vals = [ float(x) for x in args[:4]]
			mtx = numpy.matrix([
				[   vals[3],   vals[0],   vals[1],   vals[2]],
				[ - vals[0],   vals[3], - vals[2],   vals[1]],
				[ - vals[1],   vals[2],   vals[3], - vals[0]],
				[ - vals[2], - vals[1],   vals[0],   vals[3]]])
		elif tfmtype in ('mat', 'matrix'):
			mtx = numpy.matrix(' '.join(args))
		else:
			warn('transform: Unrecognized transform type "{}"', tfmtype)
		if None != mtx:
			for idx in indexes:
				pos = f3bd.points[idx].position
				pos = [[x] for x in pos]
				if 3 == mtx.shape[0] and 3 == mtx.shape[1]:
					pos = mtx * numpy.matrix(pos)
				else:
					pos = mtx * numpy.matrix(pos + [[ 1.0 ]])
				pos = pos.A1
				if 4 == len(pos):
					factor = pos[3]
					pos = [x / factor for x in pos]
				pos = pos[:3]
				f3bd.points[idx].position = pos
	elif cmd in ('setg', 'set_global'):
		for arg in aexpr:
			key, val = arg.split('=', 1)
			if 'header' == key:
				f3bd.header = bytes.fromhex(val)
			else:
				warn('set_global: Unrecognized key {}', key)
	else:
		warn('Unrecognized command {}', cmd)

parser = argparse.ArgumentParser(description='Metaio F3B file manipulator.', formatter_class=argparse.RawDescriptionHelpFormatter, epilog='''
examples:
  # Dump content of map0.f3b
  %(prog)s --dump map0.f3b
  # Combine map1.f3b and map2.f3b and write to map0.f3b
  %(prog)s -o map0.f3b map1.f3b map2.f3b
  # Drop the first ten and last ten points after combining map1.f3b and
  # map2.f3b and write to map1.f3b
  %(prog)s -e 'delete :10' -e 'delete -10:' -o -- map1.f3b map2.f3b
  # Insert a point before point 5 and dump it, but don't write to any file
  %(prog)s --dump -e 'insert 5 144.971 -18.5117 19.8387 "01 24 00 00 00 01 17 15 04 06 06 04 04 11 0f 0d 24 19 1f 07 1e 1a 18 10 1a 10 0a 0f 08 0b 0b 11 0e 19 1d 18 1e 0b 15 1f 1c 05 00 00 00 00 00 00 00"' map1.f3b
  # Append a point
  %(prog)s --dump -e 'append 144.971 -18.5117 19.8387 "01 24 00 00 00 01 17 15 04 06 06 04 04 11 0f 0d 24 19 1f 07 1e 1a 18 10 1a 10 0a 0f 08 0b 0b 11 0e 19 1d 18 1e 0b 15 1f 1c 05 00 00 00 00 00 00 00"' map1.f3b
  # Set X of point 3, 5-8 and last 5 points to 10.0, increase Y by 10,
  # decrease Z by 10 ("*=" and "/=" works, by the way)
  %(prog)s --dump -e 'set 3 5:9 -5: x=10.0 y+=10 z-=10' map1.f3b
  # Set extra data of point 5
  %(prog)s --dump -e 'set 5 extra_data="01 24 00 00 00 01 10 15 19 0d 19 1c 0a 1d 23 13 27 14 0c 17 13 13 15 1a 1b 14 13 09 14 13 07 18 19 1e 07 25 1e 04 22 16 10 1e 00 05 00 00 00 00 00"' map1.f3b
  # Transform
  %(prog)s --dump -e 'transform 0:5 matrix 1 0 0; 0 1 0; 0 0 1' map1.f3b
  %(prog)s --dump -e 'transform 0:5 matrix 1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1' map1.f3b
  %(prog)s --dump -e 'transform 0:5 quaternion 0 0 0 1' map1.f3b
  %(prog)s --dump -e 'transform 0:5 euler 0 0 180' map1.f3b
  # Set file header
  %(prog)s --dump -e 'set_global header="22 48 9a 00 0a 00 00 00 01 00 00 01"' map1.f3b
''')
parser.add_argument('path', nargs='+', help='Paths of files to read.')
parser.add_argument('-e', '--expression', action='append', help='Expression to execute.')
parser.add_argument('-o', '--output-file', nargs='?', default=argparse.SUPPRESS, help='Path of output file. Empty to write to first input file.')
parser.add_argument('--dump', action='store_true', help='Dump contents of output file.')
args = parser.parse_args()

# Read source data
dataset = read_input(args)

# Generate output

# Combine everything firstly
out = (dataset[0] if 1 == len(dataset) else F3BData.combine(*(d for d in dataset)))

# Execute expressions
if args.expression:
	for expr in args.expression:
		eval_expr(out, shlex.split(expr))

# Write output data
if args.dump or (1 == len(dataset) and not args.expression and 'output_file' not in args):
	out.dump()

if 'output_file' in args:
	output_file = args.output_file
	if None == output_file:
		output_file = args.path[0]
	output_3dmap = (output_file.endswith('.3dmap') or output_file.endswith('.zip'))
	out_data = out.to_data()
	if output_3dmap:
		for f3bd in dataset:
			if f3bd.tracking_cfg:
				bio = io.BytesIO()
				z = zipfile.ZipFile(bio, mode='w', compression=zipfile.ZIP_DEFLATED)
				z.writestr(F3B_3DMAP_F3B_FNAME, out_data)
				z.writestr(F3B_3DMAP_TRACKCFG_FNAME, f3bd.tracking_cfg)
				z.close()
				out_data = bio.getbuffer()
				break
		else:
			raise BadInputException('Missing tracking configuration, cannot write .3dmap file.')

	f = None
	if '-' == output_file:
		f = sys.stdout
	else:
		f = open(output_file, 'wb')
	if f:
		f.write(out_data)
		f.close()
