#!/bin/bash

FNAME=$(basename -s .3dmap "$1")
TMPDIR="/tmp/$FNAME"

mkdir -p "$TMPDIR"
unzip -o "$1" -d "$TMPDIR"
mv "$TMPDIR/map0.f3b" "$FNAME".f3b
