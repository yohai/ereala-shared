#!/bin/bash

FNAME=$(basename -s .f3b "$1")
TMPDIR="/tmp/$FNAME"

mkdir -p "$TMPDIR"
cp "$1" "$TMPDIR/map0.f3b"
if [[ "$2" =~ '.3dmap' ]]; then
	unzip -o "$2" -d "$TMPDIR"
elif [[ "$2" =~ '.xml' ]]; then
	cp "$2" "$TMPDIR/TrackingData_ML3D.xml"
fi
cd "$TMPDIR"
rm "$OLDPWD/$FNAME".3dmap
zip "$OLDPWD/$FNAME".3dmap map0.f3b TrackingData_ML3D.xml
cd -
