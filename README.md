Believe me, I don't like Markdown, at all. :-D

This repository stores whatever I wish to share with my colleagues. Aside from the menus for lunch, it mostly consists of materials and scripts I wrote about Unity and Mateio SDK, upon which my colleagues and me are primarily working. You are welcome to use any of the materials, and unless otherwise indicated, they are released under [Do What the Fuck You Want to Public License](http://en.wikipedia.org/wiki/WTFPL), should they be somehow useful for you. Feel free to use the issue tracker when you have something to say.

Prepare a [AsciiDoc](http://www.methods.co.nz/asciidoc/) compiler, in which all documents except this one are written.
